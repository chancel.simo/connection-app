import { ConnectedUserDto } from "../guards/guard.service";
import { UserDto } from "../services/api-client.generated";


export class AuthData {

    public static currentUser: ConnectedUserDto;
    public static isConnected: boolean = false;
}