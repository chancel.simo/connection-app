import { Injectable } from "@angular/core";
import { DialogService } from "../app/components/dialog/dialog.service";
import { AppDataService, CityDto } from "../services/api-client.generated";

@Injectable()
export class GlobalDataService {
    public static cities: CityDto[] = [];
    public allDataLoad = false;

    public static TransportType: { value: number; label: string; }[] = [
        { label: "Bus", value: 1 },
        { label: "Train", value: 2 },
        { label: "Avion", value: 3 }
    ];

    public constructor(
        private appDataService: AppDataService,
        private dialogService: DialogService,
    ) { }

    public async init() {
        if (this.allDataLoad)
            return;

        await this.loadCities();
    }

    private async loadCities() {
        this.allDataLoad = false;
        const response = await this.appDataService.getCities().toPromise();
        if (!response.Success) {
            this.dialogService.showDialog(response.Message);
            return;
        }

        GlobalDataService.cities = response.Cities;
        this.allDataLoad = true;
    }

    public async waitForGlobalDataLoaded(): Promise<boolean> {
        if (this.allDataLoad)
            return true;

        await this.sleep(500);
        return await this.waitForGlobalDataLoaded();
    }

    private sleep(delay: number) {
        return new Promise(r => setTimeout(r, delay));
    }

}