export class GlobalAppService {
    public static userHasRole(roleName: string, requireRoles: string[]) {
        if (!roleName || !requireRoles.length)
            return;

        return requireRoles.some(x => x === roleName);
    }

    public static clone<T>(source: any) {
        return JSON.parse(JSON.stringify(source)) as T;
    }
}