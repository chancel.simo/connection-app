/**
 * TRAVEL API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { UserDto } from './userDto';
import { TransportType } from './transportType';


export interface TrajetDto { 
    ID?: number;
    DepartCity?: string | null;
    ArrivalCity?: string | null;
    DepartureTime?: Date;
    ArrivalTime?: Date | null;
    Price?: number;
    TransportType?: TransportType;
    EmployeeID?: number;
    Employee?: UserDto;
}

