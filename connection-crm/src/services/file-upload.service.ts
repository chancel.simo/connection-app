
import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class FileUploadService {

    public static imagePreview: string;
    baseApiUrl = environment.ApiBaseUrl;
    constructor(private http: HttpClient) { }

    public uploadTemp(file: File): Promise<{ filename: string; filepath: string }> {
        const formData = new FormData();
        formData.append("file", file, file.name);
        //upload to api;

        return new Promise(
            (resolve, reject) => {
                this.http.post(this.baseApiUrl + "/api/Media/", formData, {
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(event => {
                    if (event.type === HttpEventType.UploadProgress) {
                        console.log("🚀 ~ progress : ", Math.round(event.loaded / event.total * 100) + '%');
                    }
                    else if (event.type === HttpEventType.Response) {
                        console.log("🚀 ~ event ", event);
                        const messageResponse: { filename: string, originalname: string } = JSON.parse(event.body["message"]);
                        resolve({ filename: messageResponse.filename, filepath: this.baseApiUrl + "/files/TempUpload/" + messageResponse.filename });
                    }

                });
            }
        );

    }
}