import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import jwt_decode from "jwt-decode";
import { CookieService } from "ngx-cookie-service";
import { DialogService } from "../app/components/dialog/dialog.service";
import { RouteList } from "../app/route-list";
import { AuthData } from "../global/auth-data";
import { UserDto, UserService } from "../services/api-client.generated";

export interface ConnectedUserDto {
    Email: string;
    FullName: string;
    ID: number;
    Role: string;
    exp: number;
    AgencyLogo: string;
}


@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
    constructor(
        private _router: Router,
        private usersService: UserService,
        private dialogService: DialogService,
        private cookieService: CookieService
    ) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        await this.getAuthTokenAccess();
        if (!AuthData.isConnected) {
            this._router.navigate(['/login']);
            return false;
        }
        return true;
    }

    private async getAuthTokenAccess() {
        const access_token = localStorage.getItem('access_token');

        const refresh_token = this.cookieService.get('travel_refresh_token');
        if (access_token && refresh_token) {
            AuthData.isConnected = true;
            AuthData.currentUser = jwt_decode(access_token) as ConnectedUserDto;
            const tokenExpired = new Date(AuthData.currentUser.exp * 1000).getTime() < Date.now();
            if (tokenExpired)
                await this.refreshToken();

        }
        else if (!access_token) {
            AuthData.isConnected = false;
            this._router.navigate([RouteList.LOGIN]);
        }
        else if (!refresh_token) {
            AuthData.isConnected = false;
        }
        else {
            AuthData.isConnected = false;
            await this.refreshToken();
        }
    }

    private async refreshToken() {
        const response = await this.usersService.refreshToken().toPromise();
        console.log("🚀 ~ refreshToken ~ response", response);
        if (!response.Success) {
            AuthData.isConnected = false;
            localStorage.removeItem('access_token');
            const logoutResponse = await this.usersService.logout().toPromise();
            if (logoutResponse.Success) {
                this._router.navigate([RouteList.LOGIN]);
                return;
            }

            this.dialogService.showDialog(logoutResponse.Message as string);
            return;
        }

        localStorage.setItem('access_token', response.AccessToken as string);
        AuthData.isConnected = true;
        AuthData.currentUser = jwt_decode(response.AccessToken as string) as ConnectedUserDto;
    }
}