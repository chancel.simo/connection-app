import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

const noop = () => {
};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CustomAutocompleteComponent),
  multi: true,
};

@Component({
  selector: 'app-custom-autocomplete',
  templateUrl: './custom-autocomplete.component.html',
  styleUrls: ['./custom-autocomplete.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class CustomAutocompleteComponent implements OnInit {

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  private innerValue: any;

  get value(): any {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }


  customControl = new FormControl();

  private _options: any[] = [];

  get options(): any[] {
    return this._options;
  }

  @Input() set
    options(value: any[]) {
    this._options = value;
  }

  @Input()
  displayLabel!: any;

  @Input()
  placeHolder!: string;

  filteredOptions!: Observable<any[]>;

  constructor() { }

  ngOnInit(): void {

    this.filteredOptions = this.customControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const label = typeof value === 'string' ? value : this.displayLabel;
        return label ? this._filter(label as string) : this._options.slice();
      })
    );
  }

  private _filter(label: string): any[] {
    const filterValue = label.toLowerCase();

    return this._options.filter(option => {
      console.log("🚀 ~ _filter ~ this.displayLabel", this.displayLabel)

      console.log("🚀 ~ _filter ~ option[this.displayLabel]", option[this.displayLabel]);

      return (option[this.displayLabel] as string).toLowerCase().includes(filterValue)
    })
  }

  displayFn(item: any): string {
    return item && item[this.displayLabel] ? item[this.displayLabel] : '';
  }

}
