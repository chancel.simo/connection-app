import { Component, OnInit, Input, forwardRef, ViewEncapsulation } from '@angular/core';
import { CityDto } from '../../../services/api-client.generated';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";


const noop = () => {
};

@Component({
  selector: 'app-city-autocomplete',
  templateUrl: './city-autocomplete.component.html',
  styleUrls: ['./city-autocomplete.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CityAutocompleteComponent),
    multi: true,
  }]
})
export class CityAutocompleteComponent implements OnInit {


  private _cities: CityDto[] = [];

  get cities(): CityDto[] {
    return this._cities;
  }

  @Input() set
    cities(v: CityDto[]) {
    this._cities = v;
  }

  @Input()
  placeHolder!: string;

  private _innerValue!: CityDto;

  get value(): CityDto {
    return this._innerValue;
  }

  @Input() set
    value(v: CityDto) {
    this._innerValue = v;
  }

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  public displayLabelFunction: (city: CityDto) => string;

  constructor() {
    this.displayLabelFunction = (city: CityDto) => {
      return !city || !city.CityValue ? '' : city.CityValue.Label as string;
    }
  }

  ngOnInit(): void {
  }

  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this._innerValue) {
      this._innerValue = value;
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  /* onTextChanged(val: string) {
      if (!val)
          this.value = null;
  } */

  /* public onClearInputClick() {
      this.clearInputClick.emit();
  } */



}
