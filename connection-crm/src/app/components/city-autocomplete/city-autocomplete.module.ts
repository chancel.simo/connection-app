import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomAutocompleteModule } from '../custom-autocomplete/custom-autocomplete.module';
import { CityAutocompleteComponent } from './city-autocomplete.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CityAutocompleteComponent
  ],
  imports: [
    CommonModule,
    CustomAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
  ],
  exports: [CityAutocompleteComponent]
})
export class CityAutocompleteModule { }
