import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthData } from '../../../global/auth-data';
import { GlobalAppService } from '../../../global/global.service';
import { UserService } from '../../../services/api-client.generated';
import { RouteList } from '../../route-list';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit {

  RouteList = RouteList;
  public authUser = AuthData.currentUser;

  @Output()
  onMenuPressed = new EventEmitter();


  constructor(
    private cookieService: CookieService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  handleMenuPressed() {
    this.onMenuPressed.emit();
  }

  async logout() {
    localStorage.removeItem('access_token');
    /* this.cookieService.delete('travel_refresh_token'); */
    const response = await this.userService.logout().toPromise();
    if (response.Success)
      this.router.navigate([this.RouteList.LOGIN]);
  }

}
