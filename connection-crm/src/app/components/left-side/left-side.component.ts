import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoleList } from '../../../../../Shared-folder/constant';
import { AuthService } from '../../../services/auth.service';
import { BaseComponent } from '../../base/base.component';
import { RouteList } from '../../route-list';

@Component({
  selector: 'app-left-side',
  templateUrl: './left-side.component.html',
  styleUrls: ['./left-side.component.scss']
})
export class LeftSideComponent extends BaseComponent {
  public RouteList = RouteList;
  public RoleList = RoleList;
  public user = this.AuthData.currentUser;

  // currentRoute?: string;
  constructor(
    private router: Router
  ) {
    super();
    console.log("🚀 ~ user", this.AuthData.currentUser);

  }

  ngOnInit(): void {
    //  this.currentRoute = this.router.url.substring(1, this.router.url.length);
  }
  /* 
    isActiveLink(link: string) {
      return link === this.currentRoute;
    } */



}
