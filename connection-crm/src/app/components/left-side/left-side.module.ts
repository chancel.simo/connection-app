import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { LeftSideComponent } from "./left-side.component";
import { MatIconModule } from "@angular/material/icon";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations: [LeftSideComponent],
    imports: [
        CommonModule,
        MatIconModule,
        RouterModule,
    ],
    exports: [LeftSideComponent]
})
export class LeftSideModule { }