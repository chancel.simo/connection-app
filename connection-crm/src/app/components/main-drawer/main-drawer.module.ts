import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MainDrawerComponent } from "./main-drawer.component";
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { LeftSideModule } from "../left-side/left-side.module";
import { HeaderModule } from "../header/header.module";


@NgModule({
    declarations: [MainDrawerComponent],
    imports: [
        CommonModule,
        MatSidenavModule,
        MatButtonModule,
        MatIconModule,
        LeftSideModule,
        HeaderModule,
    ],
    exports: [MainDrawerComponent],
})
export class MainDrawerModule { }