import { Component, Input, OnInit, Type } from '@angular/core';

@Component({
  selector: 'app-base-list',
  templateUrl: './base-list.component.html',
  styleUrls: ['./base-list.component.scss']
})
export class BaseListComponent<T> implements OnInit {

  @Input()
  baseList: T[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
