import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseListComponent } from './base-list.component';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    BaseListComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
  ]
})
export class BaseListModule { }
