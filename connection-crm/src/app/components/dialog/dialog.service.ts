import { ComponentType } from "@angular/cdk/portal";
import { Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DialogComponent } from "./dialog.component";

export interface IDialogOptions {
    content?: string;
    title?: string;
}

export interface ICustomDialogOption<T> extends MatDialogConfig {
    component: ComponentType<T>;
    data?: any;
}

@Injectable({
    providedIn: 'root',
})
export class DialogService {

    constructor(
        private _dialog: MatDialog,
        private _snackBar: MatSnackBar
    ) { }

    showDialog(message: string): MatDialogRef<DialogComponent> {
        return this._dialog.open(DialogComponent, {
            data: message,
            minWidth: 300,
            minHeight: 200
        });
    }

    showDialogAwaitable<Z = void, T = any, Y = any>(opts: ICustomDialogOption<T>): Promise<Z | undefined> {
        return new Promise<Z | undefined>((resolve, reject) => {
            const dialogRef = this._dialog.open<T, Y, Z>(opts.component, {
                data: opts.data,
                minHeight: opts.minHeight,
                minWidth: opts.minWidth,
                disableClose: opts.disableClose,
            });

            dialogRef.afterClosed().subscribe((x) => {
                resolve(x);
            });

        });
    }

    showSnackBar(message: string) {
        this._snackBar.open(message, undefined, {
            duration: 2000
        });
    }
}