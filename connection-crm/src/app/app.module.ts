import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BASE_PATH } from '../services/api-client.generated';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { CustomInterceptor } from '../services/http-interceptor';
import { AuthGuardService } from '../guards/guard.service';
import { MainDrawerModule } from './components/main-drawer/main-drawer.module';
import { MatDialogModule } from '@angular/material/dialog';
import { TripComponent } from './pages/trip/trip.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { TripDialogComponent } from './dialogs/trip-dialog/trip-dialog.component';
import { TripDialogModule } from './dialogs/trip-dialog/trip-dialog.module';
import { DialogModule } from './components/dialog/dialog.module';
import { registerLocaleData } from '@angular/common';
import * as fr from '@angular/common/locales/fr';
import { MatSelectModule } from '@angular/material/select';
import { GlobalDataService } from '../global/global-data.service';
import { CookieService } from 'ngx-cookie-service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MainDrawerModule,
    MatDialogModule,
    TripDialogModule,
    DialogModule,
    FormsModule,
    MatSelectModule,
  ],
  providers: [
    { provide: BASE_PATH, useValue: environment.ApiBaseUrl },
    { provide: HTTP_INTERCEPTORS, useClass: CustomInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    AuthGuardService,
    GlobalDataService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(fr.default);
  }
}
