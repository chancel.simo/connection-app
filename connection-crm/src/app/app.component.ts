import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../global/global-data.service';

@Component({
  selector: 'app-root',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: [`
    app-root{
      height:100%;
      width:100%;
    }
  `]
})
export class AppComponent implements OnInit {
  title = 'travel-config';

  constructor(
    private globalDataService: GlobalDataService,
  ) {
    this.loadGlobalData();

  }

  ngOnInit(): void {
  }

  async loadGlobalData() {
    await this.globalDataService.init();

    // console.log("🚀 ~ loadGlobalData ~ GlobalDataService.cities", GlobalDataService.cities);
  }
}
