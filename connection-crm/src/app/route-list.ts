export enum RouteList {
    HOME = "home",
    CONNECTION = "connection",
    LOGIN = "login",
    COMPTA = "compta",
    SETTING = "setting",
    TRIP = "trip",
    ACCOUNT = "account",
    STATISTICS = "stat",
    REGISTER_WITH_GUID = "register-with-guid",
}