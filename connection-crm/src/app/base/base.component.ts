import { Directive } from "@angular/core";
import { RoleList } from "../../../../Shared-folder/constant";
import { AuthData } from "../../global/auth-data";
import { GlobalAppService } from "../../global/global.service";
import { RouteList } from "../route-list";

@Directive()
export abstract class BaseComponent {
    public loading: boolean = false;
    public AuthData = AuthData;

    public GlobalAppService = GlobalAppService;
    public RouteList = RouteList;
    public RoleList = RoleList;
}