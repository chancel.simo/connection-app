import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalAppService } from '../../../global/global.service';
import { ConnectionDto, PriceTripDto, TripDto } from '../../../services/api-client.generated';


@Component({
  selector: 'app-price-trip-dialog',
  templateUrl: './price-trip-dialog.component.html',
  styleUrls: ['./price-trip-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PriceTripDialogComponent implements OnInit {
  errorMessages: string[] = []
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public tripEntity: TripDto,
    private dialogRef: MatDialogRef<PriceTripDialogComponent>
  ) {
  }

  ngOnInit(): void {
    // delete this.tripEntity.ID;

    /* if (!this.tripEntity.PriceTrips?.length)
      this.tripEntity.PriceTrips = [{}]; */
    console.log("🚀 ~ ngOnInit ~ this.tripEntity", this.tripEntity);
    if (!this.tripEntity.PriceTrips?.length)
      this.preparePriceTripWrapper();

  }

  validate() {
    this.errorMessages = [];
    if (this.tripEntity?.PriceTrips?.some(x => !x.Price))
      this.errorMessages.push("vous devez saisir tout les prix.");

    if (this.errorMessages.length)
      return;

    this.tripEntity.PriceTrips = this.tripEntity.PriceTrips;

    //TODO Au cas ou
    /* this.tripEntity.PriceTrips.forEach(x => {
      x.TripID = id;
    }); */

    console.log("🚀 ~ connection", this.tripEntity);

    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close();
  }

  preparePriceTripWrapper() {
    if (!this.tripEntity.PriceTrips?.length)
      this.tripEntity.PriceTrips = [];

    let i = 0;
    for (const arret of this.tripEntity.ListArrets!) {
      const listArrets = this.tripEntity.ListArrets!;

      if (!listArrets[i + 1])
        break;
      const priceTripList: PriceTripDto[] = [
        {
          DepartArretID: arret.ID!,
          DepartArret: arret,
          ArrivalArretID: listArrets[i + 1].ID!,
          ArrivalArret: listArrets[i + 1],
          TripID: arret.TripID,
        }
      ];

      for (let j = i + 1; j <= listArrets.length; j += 1) {
        if (!listArrets[j + 1])
          break;

        priceTripList.push({
          DepartArretID: arret.ID!,
          DepartArret: arret,
          ArrivalArretID: listArrets[j + 1].ID!,
          ArrivalArret: listArrets[j + 1],
          TripID: arret.ID,
        })
      }

      this.tripEntity.PriceTrips.push(...priceTripList);
      i += 1;
    }
  }

}
