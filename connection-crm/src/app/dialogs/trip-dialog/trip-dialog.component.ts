import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface TripDialogData {
  connectionId: number;
}

@Component({
  selector: 'app-trip-dialog',
  templateUrl: './trip-dialog.component.html',
  styleUrls: ['./trip-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TripDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: TripDialogData,
    public dialogRef: MatDialogRef<TripDialogComponent>
  ) {
    console.log("🚀 ~ data", data)
  }

  ngOnInit(): void {
  }

  onClose() {
    this.dialogRef.close();
  }

}
