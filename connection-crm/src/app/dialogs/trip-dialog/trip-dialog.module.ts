import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TripDialogComponent } from './trip-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { DialogService } from '../../components/dialog/dialog.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [TripDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [DialogService]
})
export class TripDialogModule { }
