import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../guards/guard.service';
import { RouteList } from './route-list';

const routes: Routes = [
  {
    path: RouteList.LOGIN,
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
    pathMatch: 'full'
  },
  {
    path: RouteList.HOME,
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.CONNECTION,
    loadChildren: () => import('./pages/connection/connections-list.module').then(m => m.ConnectionListModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.CONNECTION + '/:id',
    loadChildren: () => import('./pages/edit-connection/edit-connection.module').then(m => m.EditConnectionModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.TRIP,
    loadChildren: () => import('./pages/trip/trip.module').then(m => m.TripModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.ACCOUNT,
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.SETTING,
    loadChildren: () => import('./pages/setting/setting.module').then(m => m.SettingModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.REGISTER_WITH_GUID,
    loadChildren: () => import('./pages/register-token/register-token.module').then(m => m.RegisterTokenModule),
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
