import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RegisterTokenComponent } from "./register-token.component";
import { RouterModule } from "@angular/router";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { FormsModule } from "@angular/forms";


const Routes = [
    {
        path: '',
        component: RegisterTokenComponent
    }
];

@NgModule({
    declarations: [RegisterTokenComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(Routes),
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatButtonModule,
        FormsModule
    ],
    exports: [RegisterTokenComponent]
})
export class RegisterTokenModule {
}