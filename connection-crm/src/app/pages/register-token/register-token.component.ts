import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserDto, UserService } from "../../../services/api-client.generated";
import { DialogService } from "../../components/dialog/dialog.service";
import { RouteList } from "../../route-list";

@Component({
    selector: 'app-register-token',
    templateUrl: './register-token.component.html',
    styleUrls: ['../login/login.component.scss', '../../base/base.component.scss']
})
export class RegisterTokenComponent implements OnInit {
    loading = false;
    loginToken: string;
    user: UserDto;
    messageApi: string;
    confirmPassword: string;
    constructor(
        private userService: UserService,
        private dialogServie: DialogService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.route.queryParams.subscribe((param) => {
            this.loginToken = param["token"];
        })
    }

    async ngOnInit() {
        await this.init();
    }

    async init() {
        await this.loadUser();
    }

    private async loadUser() {
        this.loading = true;
        const response = await this.userService.getUserByToken(this.loginToken).toPromise();
        this.loading = false;
        if (!response.Success) {
            this.messageApi = response.Message;
            return;
        }

        this.user = response.User;
        console.log("🚀 ~ loadUser ~ this.user", this.user);
    }

    beforeSaveCheck(): string[] {
        const errors: string[] = [];

        if (!this.user.Password || !this.confirmPassword)
            errors.push("Veuillez remplir correctement tout les champs");

        if (this.user.Password != this.confirmPassword)
            errors.push("Les mots de passes ne correspondent pas.");

        if (errors.length)
            this.messageApi = errors.join("");


        this.loading = false;
        return errors;
    }

    async onValidate() {
        this.loading = true;
        if (this.beforeSaveCheck().length)
            return;

        const response = await this.userService.registerWithToken(this.user).toPromise();
        if (!response.Success) {
            this.messageApi = response.Message;
            return;
        }

        this.router.navigate([RouteList.LOGIN]);
    }

}