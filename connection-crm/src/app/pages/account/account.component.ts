import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { RoleList } from '../../../../../Shared-folder/constant';
import { ConnectionDto, ConnectionService, UserDto, UserService } from '../../../services/api-client.generated';
import { BaseComponent } from '../../base/base.component';
import { DialogService } from '../../components/dialog/dialog.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss', '../../base/base.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AccountComponent extends BaseComponent implements OnInit {
  user: UserDto;
  passwordType: "password" | "text" = "password";
  showPasswordInput: boolean = false;
  constructor(
    private userService: UserService,
    private dialogService: DialogService,
  ) {
    super();
    this.user = {};
    this.loadUser();
  }

  async ngOnInit() {
  }

  async loadUser() {
    this.loading = true;

    const userResponse = await this.userService.getUser(this.AuthData.currentUser.ID).toPromise();
    this.loading = false;
    if (!userResponse.Success) {
      this.dialogService.showDialog(userResponse.Message as string);
      return;
    }

    this.user = userResponse.User as UserDto;
    console.log("🚀 ~ loadUser ~ this.user", this.user)

  }

  async onAddAuto() {
    if (!this.user.Agency?.Vehicles?.length)
      this.user.Agency!.Vehicles = [];

    this.user.Agency?.Vehicles?.push({});
  }

  async saveUser() {
    this.loading = true;

    const response = await this.userService.saveUser(this.user).toPromise();
    this.loading = false;
    if (!response.Success) {
      this.dialogService.showDialog(response.Message as string);
      return;
    }

    this.dialogService.showSnackBar("sauvegardé avec succès");


  }

  showHidePassword() {
    if (this.passwordType === "password")
      this.passwordType = "text";
    else
      this.passwordType = "password";
  }

}
