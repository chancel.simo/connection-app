import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MainDrawerModule } from "../../components/main-drawer/main-drawer.module";
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { AccountComponent } from "./account.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule } from "@angular/forms";


const Routes = [
    {
        path: '',
        component: AccountComponent
    }
];

@NgModule({
    declarations: [AccountComponent],
    imports: [
        CommonModule,
        MainDrawerModule,
        RouterModule.forChild(Routes),
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatTabsModule,
        FormsModule,
        MatTableModule,
    ]
})

export class AccountModule { }