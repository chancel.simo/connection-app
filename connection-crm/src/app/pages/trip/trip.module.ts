import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TripComponent } from './trip.component';
import { RouterModule } from '@angular/router';
import { MainDrawerModule } from '../../components/main-drawer/main-drawer.module';
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";



const Routes = [
  {
    path: '',
    component: TripComponent
  }
];

@NgModule({
  declarations: [
    TripComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(Routes),
    MainDrawerModule,
    MatIconModule,
    MatButtonModule,
  ]
})
export class TripModule { }
