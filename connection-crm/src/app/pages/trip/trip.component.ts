import { Component, OnInit } from '@angular/core';
import { TripDto, TripService } from '../../../services/api-client.generated';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.scss']
})
export class TripComponent extends BaseComponent implements OnInit {
  tripList: TripDto[] = [];

  constructor(
    private tripService: TripService,
  ) {
    super();
  }

  async ngOnInit() {
    await this.loadTrip();
  }

  async loadTrip() {
    this.loading = true;

    const response = await this.tripService.getTripList().toPromise();
    console.log("🚀 ~ loadTrajet ~ response", response)
    this.loading = false;

    if (!response?.Success) {
      return;
    }

    this.tripList = response.TripsList as TripDto[];
  }

}
