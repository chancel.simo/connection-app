import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { AppTypeConstant } from '../../../../../Shared-folder/constant';
import { GlobalDataService } from '../../../global/global-data.service';
import { AppDataService, AppValueDto, ArretDto, AutoDto, CityDto, ConnectionDto, ConnectionService, FrequenceDto, PriceTripDto, TripDto, UserDto, UserService } from '../../../services/api-client.generated';
import { BaseComponent } from '../../base/base.component';
import { CityInputComponent } from '../../components/city-input/city-input.component';
import { DialogService } from '../../components/dialog/dialog.service';
import { PriceTripDialogComponent } from '../../dialogs/price-trip-dialog/price-trip-dialog.component';
import { TripDialogComponent, TripDialogData } from '../../dialogs/trip-dialog/trip-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-connection',
  templateUrl: './edit-connection.component.html',
  styleUrls: ['./edit-connection.component.scss', '../../base/base.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditConnectionComponent extends BaseComponent implements OnInit {
  mode?: "journalier" | "multi";
  connection!: ConnectionDto;
  citiesValues?: CityDto[] = [];

  selectedTrips: TripDto[] = [];

  stopOccurence: number = 0;

  panelOpenState = false;
  user: UserDto = {};
  frequenceList: FrequenceDto[] = [];
  selectedFrequence: number = 0;
  vehicleList: AutoDto[] = [];
  selectedVehicle: number = 0;
  currentConnectionId?: string;
  departureCityID?: number;
  arrivalCityID?: number;
  currentTrip!: TripDto;
  currentArret!: ArretDto;
  isTerminus: boolean = false;
  lockTrajetConfiguration: boolean = false;
  @ViewChild(CityInputComponent)
  cityInput!: CityInputComponent;

  disableCurrentArret: boolean = false;
  currentPriceTrip!: PriceTripDto;
  currentTerminus!: ArretDto;
  currentDeparture!: ArretDto;
  editMode: boolean = false;

  constructor(
    private userService: UserService,
    private dialogService: DialogService,
    private connectionService: ConnectionService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
    super();
    this.currentArret = {};

  }

  ngOnInit() {
    this.init();

  }

  private async init(): Promise<void> {
    await this.loadUser();
    this.citiesValues = GlobalDataService.cities;
    this.route.params.subscribe(params => {
      this.currentConnectionId = params.id;
      if (this.currentConnectionId !== 'new')
        this.disableCurrentArret = true;
    });

    await this.loadConnection();
  }

  addNewArret() {
    if (!this.currentTrip?.ListArrets?.length)
      this.currentTrip!.ListArrets = [];

    const index = this.currentTrip.ListArrets.findIndex(x => x.CityID === this.currentArret.CityID)
    if (index !== -1) {
      this.currentTrip.ListArrets[index] = { ...this.currentArret };
      this.currentArret = {};
      this.isTerminus = false;
      if (this.isTerminus)
        this.lockTrajetConfiguration = true;
      return;
    }
    this.currentArret.City = this.citiesValues?.find(x => x.ID === this.currentArret.CityID);

    this.currentTrip.ListArrets.push({ ...this.currentArret });

    if (this.currentTrip.ListArrets.length === 1) {
      const currentDepartureIndex = this.currentTrip.ListArrets.indexOf(this.currentArret);
      this.currentDeparture = this.currentTrip.ListArrets[currentDepartureIndex];
    }

    if (this.isTerminus) {
      this.lockTrajetConfiguration = true;

      const currentTerminusIndex = this.currentTrip.ListArrets.indexOf(this.currentArret);
      this.currentTerminus = this.currentTrip.ListArrets[currentTerminusIndex];
    }

    this.cityInput.clearInput();
    this.isTerminus = false;

    this.currentArret = {};
  }

  async loadConnection() {
    if (this.currentConnectionId === "new") {
      this.currentTrip = {
        PriceTrips: [],
        ListArrets: [],
      };

      this.connection = {
        AgentID: this.AuthData.currentUser.ID,
        Trips: []
      };

    }
    else {
      this.loading = true;
      const connectionResponse = await this.connectionService.getConnection(+this.currentConnectionId!).toPromise();
      this.loading = false;
      if (!connectionResponse.Success) {
        this.dialogService.showDialog(connectionResponse.Message!);
        return;
      }

      this.connection = connectionResponse.Connection as ConnectionDto;

      if (!this.connection)
        return;

      for (const item of this.connection.Trips!) {
        for (const arret of item.ListArrets!) {
          if (this.isValidDate(new Date(arret.ArrivalTime)))
            arret.ArrivalTime = new Date(arret.ArrivalTime!);
          else
            delete arret.ArrivalTime;

          if (this.isValidDate(new Date(arret.DepartureTime)))
            arret.DepartureTime = new Date(arret.DepartureTime!);
          else
            delete arret.DepartureTime;
        }
      }

      this.selectedVehicle = this.connection.Auto?.ID!;

      if (this.connection && this.connection.Trips?.length) {
        this.selectedFrequence = this.connection.Trips[0].Frequence?.ID!;
        this.route.queryParams.subscribe(p => {
          const tripId = +p['tripId'];
          if (tripId) {
            const tripFromParam = this.connection.Trips.find(x => x.ID === tripId);
            if (tripFromParam)
              this.currentTrip = tripFromParam;
            else
              this.currentTrip = this.connection.Trips[0];
          }
          else
            this.currentTrip = this.connection.Trips[0];
        });
        console.log("🚀 ~ loadConnection ~ this.currentTrip", this.currentTrip);
      }

    }

  }

  async loadUser() {
    this.loading = true;
    const userResponse = await this.userService.getUser(this.AuthData.currentUser.ID).toPromise();
    this.loading = false;
    if (!userResponse.Success) {
      this.dialogService.showDialog(userResponse.Message!)
      return;
    }

    this.user = userResponse.User as UserDto;
    this.frequenceList = this.user.Agency?.Frequences as FrequenceDto[];
    this.vehicleList = this.user.Agency?.Vehicles as AutoDto[];
  }




  getCityLabel(cityId: number) {
    if (!cityId || !this.citiesValues?.length)
      return "-";

    return this.citiesValues.find(x => x.ID === cityId)?.CityValue?.Label;
  }

  async saveConnection() {
    this.loading = true;

    if (!this.connection.Trips?.length)
      this.connection.Trips = [];

    this.connection.AutoID = this.selectedVehicle;

    const copyConnection = JSON.parse(JSON.stringify(this.connection)) as ConnectionDto;

    if (!this.currentTrip.ID)
      copyConnection.Trips.push(this.currentTrip);

    let i = 0;

    console.log("🚀 ~ saveConnection ~ copyConnection", copyConnection);

    for (const trip of copyConnection.Trips!) {

      trip.FrequenceID = this.selectedFrequence;
      trip.AutoID = this.selectedVehicle;

      for (const arret of trip.ListArrets!) {
        i += 1;
        if (trip.ListArrets[i] && trip.ListArrets[i].ArrivalTime! < arret.DepartureTime) {

          const date = trip.ListArrets[i].ArrivalTime;

          if (trip.ListArrets[i].ArrivalTime && this.isValidDate(trip.ListArrets[i].ArrivalTime))
            trip.ListArrets[i].ArrivalTime.setDate(date.getDate() + 1);
          if (trip.ListArrets[i].DepartureTime && this.isValidDate(trip.ListArrets[i].DepartureTime))
            trip.ListArrets[i].DepartureTime.setDate(date.getDate() + 1);
        }
      }
    }

    console.log("🚀 ~ saveConnection ~ copyConnection", copyConnection)

    //TODO FORMAT TIME FROM DAVID INTERCEPTOR
    const connectionResponse = await this.connectionService.createConnection(copyConnection).toPromise();
    this.loading = false;
    if (!connectionResponse.Success) {
      this.dialogService.showDialog(connectionResponse.Message!);
      return;
    }

    this.connection = connectionResponse.Connection;
    this.dialogService.showSnackBar('Connection enregistrée avec succès.');
    if (this.currentConnectionId === 'new')
      this.router.navigate([this.RouteList.CONNECTION, this.connection.ID]);


  }

  removeArret(arret: ArretDto) {
    const index = this.currentTrip.ListArrets?.indexOf(arret);
    if (index !== -1)
      this.currentTrip.ListArrets?.splice(index!, 1);
  }

  editArret(arret: ArretDto) {
    console.log("🚀 ~ editArret ~ arret", arret);
    this.lockTrajetConfiguration = false;
    if (arret)
      this.currentArret = { ...arret };

    const indexEditArret = this.currentTrip.ListArrets?.indexOf(arret);
    console.log("🚀 ~ editArret ~ indexEditArret", indexEditArret)

    /*  if(this.currentDeparture)
     {
       const indexCurrentTerminus = this.currentTrip.ListArrets?.indexOf(this.currentTerminus);
       
     } */

    if (this.currentTerminus) {
      console.log("🚀 ~ editArret ~ indexEditArret", indexEditArret)
      const indexCurrentTerminus = this.currentTrip.ListArrets?.indexOf(this.currentTerminus);
      console.log("🚀 ~ editArret ~ indexCurrentTerminus", indexCurrentTerminus)

      if (indexEditArret === indexCurrentTerminus)
        this.isTerminus = true;
    }


  }

  isValidDate(d: any) {
    return d instanceof Date && !isNaN(d as any);
  }

  async openPriceDialog(trip: TripDto) {
    const response = await this.dialogService.showDialogAwaitable<boolean>(
      {
        component: PriceTripDialogComponent,
        data: trip,
        minWidth: 500,
        minHeight: 700
      });

    if (response)
      console.log("🚀 ~ response.afterClosed ~ this.connection", this.connection);

  }

  addNewTrip() {
    if (!this.connection.Trips?.length) {
      this.connection.Trips = [];
    }

    this.currentTrip = {
      ConnectionID: this.connection.ID,
      ListArrets: [],
    }

    for (const arret of this.connection.Trips[0].ListArrets!) {
      const cloneArret = { ...arret };

      cloneArret.ID = null;
      cloneArret.DepartureTime = null;
      cloneArret.ArrivalTime = null;
      cloneArret.TripID = null;

      this.currentTrip.ListArrets?.push(cloneArret);
    }


    console.log("🚀 ~ addNewTrip ~ this.currentTrip.ListArrets", this.currentTrip)

  }

  addNewStop() {
    console.log("🚀 ~ addNewStop ~ this.currentTrip", this.currentTrip)

    if (!this.currentTrip || !this.currentTrip?.ListArrets)
      return;

    if (!this.currentTrip?.ListArrets?.length)
      this.currentTrip.ListArrets = [];

    this.currentTrip.ListArrets.push({});
  }

  removeStop(arret: ArretDto) {
    if (!arret)
      return;

    const index = this.currentTrip.ListArrets.indexOf(arret);
    if (index !== -1)
      this.currentTrip.ListArrets.splice(index, 1);
  }

  validateChangeOnTrip() {
    this.editMode = !this.editMode;
    if (!this.currentTrip || !this.currentTrip.ListArrets?.length)
      return;

    for (const arret of this.currentTrip.ListArrets) {
      arret.City = this.citiesValues?.find(x => x.ID === arret.CityID);
    }

  }

  setCurrentTrip(id: number) {
    this.currentTrip = this.connection.Trips.find(x => x.ID === id);
  }

  getFullPriceTrip(tripId: number, departArretID: number, arrivalArretID: number) {
    return this.connection.Trips.find(x => x.ID == tripId)?.PriceTrips?.find(y => departArretID == y.DepartArretID && y.ArrivalArretID == arrivalArretID)?.Price;
  }
}
