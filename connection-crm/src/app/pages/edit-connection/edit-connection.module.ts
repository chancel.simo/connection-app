import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material/button";
import { MatNativeDateModule, MatRippleModule } from "@angular/material/core";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { RouterModule } from "@angular/router";
import { MainDrawerModule } from "../../components/main-drawer/main-drawer.module";
import { MatExpansionModule } from '@angular/material/expansion';
import { EditConnectionComponent } from "./edit-connection.component";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { CityInputModule } from "../../components/city-input/city-input.module";
import { PriceTripDialogModule } from "../../dialogs/price-trip-dialog/price-trip-dialog.module";
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule } from "@angular/forms";
import { MatTimepickerModule } from 'mat-timepicker';


const Routes = [
  {
    path: '',
    component: EditConnectionComponent,
  }
];

@NgModule({
  declarations: [
    EditConnectionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(Routes),
    MainDrawerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatAutocompleteModule,
    CityInputModule,
    PriceTripDialogModule,
    MatTooltipModule,
    FormsModule,
    MatTimepickerModule
  ],
  providers: [
    MatDatepickerModule,
  ]
})
export class EditConnectionModule { }
