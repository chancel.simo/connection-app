import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Constant, RoleList } from '../../../../../Shared-folder/constant';
import { GlobalDataService } from '../../../global/global-data.service';
import { AgencyDto, AgencyService, ConnectionDto, ConnectionService, FrequenceDto, UserDto, UserRoleDto, UserService } from '../../../services/api-client.generated';
import { FileUploadService } from '../../../services/file-upload.service';
import { BaseComponent } from '../../base/base.component';
import { DialogService } from '../../components/dialog/dialog.service';
import { HttpClient } from '@angular/common/http';
import { GlobalAppService } from '../../../global/global.service';
import { AuthData } from '../../../global/auth-data';


interface AgencySection {
  frequences?: boolean;
  auto?: boolean;
  employee?: boolean;
}

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss', '../../base/base.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SettingComponent extends BaseComponent {
  currentMonth: any;
  frequenceDayKey: string[] = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
  //agency: AgencyDto;
  selectAllModel: boolean;
  currentFrequence: FrequenceDto;
  editModes: AgencySection = { frequences: false, auto: false, employee: false }
  employeeList: UserDto[];
  currentEditEmployee: UserDto;
  displayedColumns: string[] = ['LastName', 'FirstName', 'Email', 'Role', 'Statut', "Action"];
  displayedColumnsAgencies: string[] = ['Name', 'Ref', 'TransportType', 'Logo', 'Action'];
  roleListWrapper: UserRoleDto[] = [];
  confirmPassword: string;
  passwordType: "password" | "text" = "password";
  public TransportType = GlobalDataService.TransportType;

  agencyLogo: File = null;
  imagePreview: string;

  public agencies: AgencyDto[];
  public cloneAgencies: AgencyDto[];

  currentAgency: AgencyDto;

  constructor(
    private globalDataService: GlobalDataService,
    private agencyService: AgencyService,
    private dialogService: DialogService,
    private fileUploadService: FileUploadService,
  ) {
    super();
    this.currentAgency = {};
    // this.init();
  }

  async ngOnInit() {
    await this.init();
  }

  async init() {
    /* this.loading = true;
    await this.globalDataService.waitForGlobalDataLoaded();

    const agencyResponse = await this.agencyService.getConnectedUserAgency().toPromise();
    this.loading = false;
    if (!agencyResponse.Success) {
      this.dialogService.showDialog(agencyResponse.Message)
      return;
    }

    this.agency = agencyResponse.Agency;
 */
    await this.loadAgencies();

    //await this.loadAgencyEmployees();
  }

  async loadAgencies() {
    this.loading = true;

    const agenciesResponse = await this.agencyService.getAgencies().toPromise();
    this.loading = false;
    if (!agenciesResponse.Success) {
      this.dialogService.showDialog(agenciesResponse.Message);
      return;
    }

    this.agencies = agenciesResponse.Agencies;
    // this.cloneAgencies = GlobalAppService.clone<AgencyDto[]>(this.agencies);
    for (const role of Constant.defaultRoles) {
      this.roleListWrapper.push({
        Role: {
          RoleCode: role.RoleCode,
          RoleName: role.RoleName
        }
      })
    }


    this.currentAgency = this.agencies.find(x => x.Employees.some(y => y.ID === +AuthData.currentUser.ID));
  }

  addFrequence() {
    if (!this.currentAgency.Frequences?.length)
      this.currentAgency.Frequences = [];

    this.currentAgency.Frequences.push(this.currentFrequence);
  }

  async saveAgencies() {
    this.loading = true;

    for (const agency of this.agencies) {
      const response = await this.agencyService.saveAgency(agency).toPromise();
      console.log("🚀 ~ saveAgency ~ response", response);
      this.loading = false;
      if (!response?.Success) {
        this.dialogService.showDialog(response.Message!);
        return;
      }
    }


    this.dialogService.showSnackBar("Paramètres agence enregistrées avec succès.");
  }

  selectOrUnselectAll() {
    if (!this.selectAllModel)
      this.selectAll(false);
    else
      this.selectAll(true);
  }

  private selectAll(select: boolean) {
    for (const item of this.frequenceDayKey) {
      if (select)
        this.currentFrequence[item] = true;
      else
        this.currentFrequence[item] = false;
    }
  }

  async onAddAuto() {
    if (!this.currentAgency?.Vehicles?.length)
      this.currentAgency.Vehicles = [];

    this.currentAgency?.Vehicles?.push({});
  }

  editModeClick(key: keyof AgencySection) {
    if (this.editModes[key]) {
      switch (key) {
        case 'frequences':
          this.addFrequence();
          break;
        case 'employee':
          this.addEmployee();
      }
      this.editModes[key] = false;
    }
    else {
      this.handleEditModeClick(key);
      this.editModes[key] = true;
    }
  }

  private handleEditModeClick(key: keyof AgencySection) {
    switch (key) {
      case 'frequences':
        this.currentFrequence = {};
        break;
      case 'employee':
        this.currentEditEmployee = {};
    }

  }

  isCurrentFrequenceComplete() {
    return !(this.currentFrequence != null && (!this.currentFrequence.Name || !this.currentFrequence.StartDate || !this.currentFrequence.EndDate));
  }

  isCurrentEmployeeComplete() {
    return !(this.currentEditEmployee != null && (!this.currentEditEmployee.LastName || !this.currentEditEmployee.Email));
  }

  removeFrequence(frequence: FrequenceDto) {
    const index = this.currentAgency.Frequences.indexOf(frequence);
    if (index !== -1)
      this.currentAgency.Frequences.splice(index, 1);
  }

  editFrequence(frequence: FrequenceDto) {
    this.currentFrequence = frequence;

    this.editModes.frequences = true;
  }

  editUser(user: UserDto) {
    this.currentEditEmployee = user;

    this.editModes.employee = true;
  }

  private addEmployee() {

    if (!this.currentAgency.Employees?.length)
      this.currentAgency.Employees = []

    const index = this.currentAgency.Employees.indexOf(this.currentEditEmployee);
    if (index !== -1) {
      this.currentAgency.Employees[index] = this.currentEditEmployee;
      return;
    }

    this.currentAgency.Employees.push(this.currentEditEmployee);
  }

  getRoleList(userRoles: UserRoleDto[]) {
    if (!userRoles || !userRoles.length)
      return '-';

    return userRoles.map(x => x.Role.RoleCode);
  }

  compareUserRoles(ur1: UserRoleDto, ur2: UserRoleDto) {
    return ur1.Role.RoleCode === ur2.Role.RoleCode;
  }

  showHidePassword() {
    if (this.passwordType === "password")
      this.passwordType = "text";
    else
      this.passwordType = "password";
  }

  async onFileSelected(event: any) {
    this.agencyLogo = <File>event.target.files[0];
    const response = await this.fileUploadService.uploadTemp(this.agencyLogo);
    console.log("🚀 ~ onFileSelected ~ response", response);
    this.imagePreview = response.filepath;

    this.currentAgency.LogoPath = response.filepath;
    this.currentAgency.LogoName = response.filename;
  }

  getTransportType(value: number): string {
    return this.TransportType.find(x => x.value === value).label
  }

  onAddAgencies() {
    if (!this.agencies.length)
      this.agencies = [];

    this.currentAgency = {};

    this.agencies.push(this.currentAgency);
  }

  onEditAgency(agency: AgencyDto) {
    this.currentAgency = agency;
  }

  getValidateAgencies(): AgencyDto[] {
    if (!this.agencies?.length)
      return;
    return this.agencies.filter(x => x.ID);
  }

}
