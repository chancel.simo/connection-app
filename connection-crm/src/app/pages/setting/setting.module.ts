import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MainDrawerModule } from "../../components/main-drawer/main-drawer.module";
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule } from "@angular/forms";
import { SettingComponent } from "./setting.component";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from "@angular/material/core";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatSelectModule } from "@angular/material/select";
import { MatTooltipModule } from "@angular/material/tooltip";



const Routes = [
    {
        path: '',
        component: SettingComponent
    }
];

@NgModule({
    declarations: [SettingComponent],
    imports: [
        CommonModule,
        MainDrawerModule,
        RouterModule.forChild(Routes),
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatTabsModule,
        FormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSlideToggleModule,
        MatTableModule,
        MatSelectModule,
        MatTooltipModule,
    ],
    providers: [
        MatDatepickerModule
    ]
})

export class SettingModule { }