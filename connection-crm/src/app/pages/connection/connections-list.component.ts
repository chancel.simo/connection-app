import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GlobalDataService } from '../../../global/global-data.service';
import { ArretDto, ConnectionDto, ConnectionService, TripDto, TripService } from '../../../services/api-client.generated';
import { BaseComponent } from '../../base/base.component';
import { DialogService } from '../../components/dialog/dialog.service';

@Component({
  selector: 'app-connection',
  templateUrl: './connections-list.component.html',
  styleUrls: ['./connections-list.component.scss']
})
export class ConnectionListComponent extends BaseComponent implements OnInit {

  connectionList: ConnectionDto[] = [];
  tripList: TripDto[] = [];
  constructor(
    private globalDataService: GlobalDataService,
    private connectionService: ConnectionService,
    private dialogService: DialogService,
    private tripService: TripService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.init();
  }

  private async init() {
    await this.globalDataService.waitForGlobalDataLoaded();
    //await this.loadTrajet();
    await this.loadAllTrips();
  }

  async loadAllTrips() {
    this.loading = true;

    const tripResponse = await this.tripService.getTripList({}).toPromise();
    this.loading = false;
    if (!tripResponse.Success) {
      this.dialogService.showDialog(tripResponse.Message!);
      return;
    }

    this.tripList = tripResponse.TripsList as TripDto[];
    console.log("🚀 ~ loadAllTrips ~ this.tripList", this.tripList);

  }

  getListArret(tripId: number) {
    return this.connectionList.filter(x => x.Trips?.some(y => y.ID === tripId));
  }

  getFullPriceTrip(tripId: number, departArretID: number, arrivalArretID: number) {
    return this.tripList.find(x => x.ID == tripId)?.PriceTrips?.find(y => departArretID == y.DepartArretID && y.ArrivalArretID == arrivalArretID)?.Price;
  }

}
