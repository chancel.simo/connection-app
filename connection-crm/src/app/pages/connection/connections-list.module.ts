import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MainDrawerModule } from "../../components/main-drawer/main-drawer.module";
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { ConnectionListComponent } from "./connections-list.component";


const Routes = [
    {
        path: '',
        component: ConnectionListComponent
    }
];

@NgModule({
    declarations: [ConnectionListComponent],
    imports: [
        CommonModule,
        MainDrawerModule,
        RouterModule.forChild(Routes),
        MatIconModule,
        MatButtonModule,
    ]
})

export class ConnectionListModule { }