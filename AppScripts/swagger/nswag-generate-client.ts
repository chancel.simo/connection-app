import * as path from 'path';
import * as fs from 'fs';
import * as colors from 'colors/safe';
import { execSync } from 'child_process';

const apiSwaggerUrl = 'http://localhost:5068/swagger/v1/swagger.json';
const swaggerCodegenConfig = path.join(__dirname, 'swagger-codegen-config.json');
const swaggerFrontOutputList: { outputName: string, outputPath: string }[] = [
    { outputName: 'connection-crm', outputPath: path.join(__dirname, '..', '..', 'connection-crm', 'src', 'services', 'api-client.generated') },
    { outputName: 'connection-web', outputPath: path.join(__dirname, '..', '..', 'connection-web', 'src', 'services', 'api-client.generated') }
];

if (!fs.existsSync(swaggerCodegenConfig)) {
    console.error(colors.red(`Impossible de générer le code client : le fichier de configuration ${swaggerCodegenConfig} n'existe pas !`));
    process.exit(1);
}

try {
    for (const output of swaggerFrontOutputList) {
        execSync(`npx openapi-generator-cli generate -i "${apiSwaggerUrl}" -g typescript-angular -c "${swaggerCodegenConfig}" -o "${output.outputPath}" --type-mappings DateTime=Date`).toString();
        console.log(colors.green(`Code client ${output.outputName} généré avec succès !`));
    }


} catch (error) {
    console.error(colors.red(`Impossible de générer le code client : ${error}`));

}
    //NodeHelpers.executeCommand(`java -jar "${swaggerCodegenBin}" generate -i "${apiSwaggerUrl}" -g typescript-angular -c "${swaggerCodegenConfig}" -o "${swaggerFrontOutput}" --type-mappings Date=Date`,
