/**
 * CONNECTION API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Auto } from './auto';
import { Trip } from './trip';
import { User } from './user';
import { City } from './city';
import { TransportType } from './transportType';


export interface Connection { 
    DepartureCityID?: number;
    DepartureCity?: City;
    ArrivalCityID?: number;
    ArrivalCity?: City;
    Price?: number;
    TransportType?: TransportType;
    AgentID?: number;
    Agent?: User;
    Trips?: Array<Trip> | null;
    AutoID?: number;
    Auto?: Auto;
    ID?: number;
}

