import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { MatDialog } from "@angular/material/dialog";
import { DialogService } from "../app/components/dialog/dialog.service";
@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    constructor(
        public dialogService: DialogService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.method === 'POST' || req.method === 'PUT') {
            this.formatToUtcDates(req.body);
        }

        const access_token = localStorage.getItem('access_token');

        if (access_token) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + access_token),
            });

            return this.handleHttpRequest(cloned, next);
        }
        else {
            return this.handleHttpRequest(req, next);
        }
    }

    private handleHttpRequest(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
                    if (evt.body && evt.body.success)
                        console.log("🚀 ~ handleHttpRequest ~ evt.body", evt.body)
                    //this.toasterService.success(evt.body.success.message, evt.body.success.title, { positionClass: 'toast-bottom-center' });
                }
            }),
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status == 403)
                        this.dialogService.showDialog("Erreur droit d'accès");

                }
                return of(err);
            })
        );
    }

    formatToUtcDates(body: any) {
        if (body === null || body === undefined) {
            return body;
        }

        if (typeof body !== 'object') {
            return body;
        }

        for (const key of Object.keys(body)) {
            const value = body[key];
            if (value instanceof Date) {
                body[key] = new Date(Date.UTC(value.getFullYear(), value.getMonth(), value.getDate(), value.getHours(), value.getMinutes()
                    , value.getSeconds()));
            } else if (typeof value === 'object') {
                this.formatToUtcDates(value);
            }
        }
    }


}