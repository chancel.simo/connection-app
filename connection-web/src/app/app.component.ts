import { Component, ViewEncapsulation } from '@angular/core';
import { AppDataService, AppValueDto, CityDto, ConnectionRequest, ConnectionService } from '../services/api-client.generated';
import { AppTypeConstant } from '../../../Shared-folder/constant';
import { DialogService } from './components/dialog/dialog.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'travel-UI';

  constructor(
  ) {
  }


}

