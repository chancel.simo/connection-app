import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarModule } from '../../components/toolbar/toolbar.module';
import { TripDetailComponent } from './trip-detail.component';



@NgModule({
  declarations: [TripDetailComponent],
  imports: [
    CommonModule,
    ToolbarModule,
  ]
})
export class TripDetailModule { }
