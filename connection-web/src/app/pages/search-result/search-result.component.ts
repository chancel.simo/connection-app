import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ActivatedRoute } from '@angular/router';
import { AppTypeConstant } from '../../../../../Shared-folder/constant';
import { AppDataService, AppValueDto, CityDto, ConnectionDto, ConnectionRequest, ConnectionService, TripDto } from '../../../services/api-client.generated';
import { BaseComponent } from '../../components/base/base.component';
import { DialogService } from '../../components/dialog/dialog.service';
import { FilterBoxComponent } from '../../components/filter-box/filter-box.component';
import { SearchBoxComponent } from '../../components/search-box/search-box.component';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchResultComponent extends BaseComponent {
  loading: boolean = false;
  connectionReq: ConnectionRequest = {};
  connectionList: ConnectionDto[] = [];
  citiesValues: CityDto[] = [];
  tripsList: TripDto[] = [];
  @ViewChild('templateFilterRef') templateFilterRef: TemplateRef<any>;
  @ViewChild('searchBox') searchBox: SearchBoxComponent;
  isBottomSheetOpen = false;
  constructor(
    private connectionService: ConnectionService,
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private appDataService: AppDataService,
    breakpointObserver: BreakpointObserver,
    private bottomSheet: MatBottomSheet
  ) {
    super(breakpointObserver);
    this.route.queryParams.subscribe((params) => {
      console.log("🚀 ~ this.route.queryParams.subscribe ~ params", params);
      if (params && (params['DepartArretID'] || params['ArrivalArretID'] || params['StartDate'])) {
        this.connectionReq = {
          DepartArretID: parseInt(params['DepartArretID']),
          ArrivalArretID: parseInt(params['ArrivalArretID']),
          StartDate: new Date(params['StartDate'])
        };
      }
    });

    this.loadCities();
  }

  async loadCities() {
    this.loading = true;
    const citiesResponse = await this.appDataService.getCities().toPromise();
    this.loading = false;
    if (!citiesResponse?.Success) {
      this.dialogService.showDialog(citiesResponse?.Message!);
      return;
    }

    this.citiesValues = citiesResponse.Cities!;

  }

  ngOnInit(): void {
  }

  loadConnectionList(items: ConnectionDto[]) {
    this.connectionList = items;
    this.tripsList = [];

    if (!this.connectionList.length)
      return;


    for (const item of this.connectionList) {
      this.tripsList.push(...item.Trips!);
    }
    console.log("🚀 ~ loadConnectionList ~ this.tripsList", this.tripsList)

  }

  getCityById(id: number) {
    if (!id)
      return;
    return this.citiesValues.find(x => x.ID == id)?.CityValue;
  }

  getTripsLength() {
    if (!this.tripsList.length)
      return;

    return this.tripsList.length;
  }

  toHHMMSS(ms: number) {
    const sec_num = ms / 1000;
    const hours = Math.floor(sec_num / 3600);
    const minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    const seconds = sec_num - (hours * 3600) - (minutes * 60);

    let hoursStr = '', minutesStr = '', secondsStr = '';
    if (hours < 10) { hoursStr = "0" + hours; }
    if (minutes < 10) { minutesStr = "0" + minutes; }
    if (seconds < 10) { secondsStr = "0" + seconds; }
    return hoursStr + ':' + minutesStr + ':' + secondsStr;
  }

  getFullPriceTrip(tripId: number, departArretID: number, arrivalArretID: number) {
    return this.tripsList.find(x => x.ID == tripId)?.PriceTrips?.find(y => departArretID == y.DepartArretID && y.ArrivalArretID == arrivalArretID)?.Price;
  }

  openFilterTemplateSheet() {
    this.isBottomSheetOpen = true;
    this.bottomSheet.open(FilterBoxComponent).afterDismissed().subscribe(() => {
      this.isBottomSheetOpen = false;
    });
  }

  onRequestChange(request: ConnectionRequest) {
    this.searchBox.connectionReq = request;
    this.searchBox.searchConnection();
  }



}
