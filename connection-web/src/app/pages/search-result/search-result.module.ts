import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResultComponent } from './search-result.component';
import { RouterModule } from '@angular/router';
import { ToolbarModule } from '../../components/toolbar/toolbar.module';
import { SearchBoxModule } from '../../components/search-box/search-box.module';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { FilterBoxComponent } from '../../components/filter-box/filter-box.component';
import { FilterBoxModule } from '../../components/filter-box/filter-box.module';
import { FooterModule } from '../../components/footer/footer.module';



const routes = [{
  path: '',
  component: SearchResultComponent
}
];

@NgModule({
  declarations: [
    SearchResultComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ToolbarModule,
    SearchBoxModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    FlexLayoutModule,
    MatBottomSheetModule,
    FilterBoxModule,
    FooterModule
  ],
  exports: [SearchResultComponent]
})
export class SearchResultModule { }
