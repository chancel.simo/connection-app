import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AppTypeConstant } from '../../../../../Shared-folder/constant';
import { AppDataService, AppValueDto, ConnectionRequest, ConnectionService } from '../../../services/api-client.generated';
import { BaseComponent } from '../../components/base/base.component';
import { DialogService } from '../../components/dialog/dialog.service';
import { RouteList } from '../../routes';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent extends BaseComponent {
  connectionReq: ConnectionRequest = {};
  citiesValues: AppValueDto[] = [];
  loading: boolean = false;
  constructor(
    private appDataService: AppDataService,
    private dialogService: DialogService,
    private connectionService: ConnectionService,
    private router: Router,
    breakpointObserver: BreakpointObserver

  ) {
    super(breakpointObserver);
    this.loadData();
  }

  async loadData() {
    await this.loadCities();
  }

  async loadCities() {
    this.loading = true;
    const citiesResponse = await this.appDataService.getAppType(AppTypeConstant.CityType).toPromise();
    this.loading = false;
    if (!citiesResponse?.Success) {
      this.dialogService.showDialog(citiesResponse?.Message!);
      return;
    }

    this.citiesValues = citiesResponse.AppType?.AppValues!;

  }

  goTosearchResult(connectionReq: ConnectionRequest) {
    this.router.navigate([RouteList.SearchResult], {
      queryParams: {
        DepartArretID: connectionReq.DepartArretID,
        ArrivalArretID: connectionReq.ArrivalArretID,
        StartDate: connectionReq.StartDate,
      }
    });
  }

  log() {
    console.log("🚀 ~ connectionReq", this.connectionReq)
  }
}
