import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteList } from './routes';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
    pathMatch: 'full'
  },
  {
    path: RouteList.HOME,
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
    pathMatch: 'full'
  },
  {
    path: RouteList.SearchResult,
    loadChildren: () => import('./pages/search-result/search-result.module').then(m => m.SearchResultModule),
    pathMatch: 'full'
  },
  {
    path: RouteList.TripDetail + '/:tripId',
    loadChildren: () => import('./pages/trip-detail/trip-detail.module').then(m => m.TripDetailModule),
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
