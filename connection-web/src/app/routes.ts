export const enum RouteList {
    HOME = 'home',
    SearchResult = 'search-result',
    TripDetail = 'trip-detail',
};