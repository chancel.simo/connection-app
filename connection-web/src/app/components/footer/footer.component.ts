import { BreakpointObserver } from "@angular/cdk/layout";
import { Component, ViewEncapsulation } from "@angular/core";
import { BaseComponent } from "../base/base.component";

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    encapsulation: ViewEncapsulation.None
})
export class FooterComponent extends BaseComponent {
    constructor(
        private breakpointObserver: BreakpointObserver
    ) {
        super(breakpointObserver);
    }


}