import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppTypeConstant } from '../../../../../Shared-folder/constant';
import { AppDataService, AppValueDto, CityDto, ConnectionDto, ConnectionRequest, ConnectionService } from '../../../services/api-client.generated';
import { RouteList } from '../../routes';
import { BaseComponent } from '../base/base.component';
import { DialogService } from '../dialog/dialog.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchBoxComponent extends BaseComponent {
  connectionReq: ConnectionRequest = {};
  @Input()
  showTitle: boolean = true;
  loading: boolean = false;
  citiesValues: CityDto[] = [];

  searchConnectionType: 'round-trip' | 'only-go' = 'only-go';

  @Output()
  handleSearchClick = new EventEmitter<ConnectionRequest>();

  @Output()
  handleConnectionResult = new EventEmitter<ConnectionDto[]>();

  @Output()
  onLoading = new EventEmitter<boolean>();

  currentRoute: string;

  minSearchDate = new Date();

  constructor(
    private appDataService: AppDataService,
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private connectionService: ConnectionService,
    private router: Router,
    breakpointObserver: BreakpointObserver
  ) {
    super(breakpointObserver);
    this.currentRoute = this.router.url;
  }

  async ngOnInit() {
    await this.loadCities();
    this.route.queryParams.subscribe(async (params) => {
      if (params && (params['DepartArretID'] || params['ArrivalArretID'] || params['StartDate'])) {
        this.connectionReq = {
          DepartArretID: parseInt(params['DepartArretID']),
          ArrivalArretID: parseInt(params['ArrivalArretID']),
          StartDate: new Date(params['StartDate'])
        };

        await this.searchConnection();
      }
    });

  }

  async loadCities() {
    this.loading = true;
    const citiesResponse = await this.appDataService.getCities().toPromise();
    console.log("🚀 ~ loadCities ~ citiesResponse", citiesResponse);
    this.loading = false;
    if (!citiesResponse?.Success) {
      this.dialogService.showDialog(citiesResponse?.Message!);
      return;
    }

    this.citiesValues = citiesResponse.Cities!;

  }

  async onSearchClick() {

    if (this.currentRoute.includes(RouteList.SearchResult))
      await this.searchConnection();
    else
      this.handleSearchClick.emit(this.connectionReq);
  }

  async searchConnection() {
    if (!this.connectionReq)
      return;

    this.loading = true;
    this.onLoading.emit(this.loading);

    const response = await this.connectionService.getConnectionList(this.connectionReq).toPromise();
    this.loading = false;
    this.onLoading.emit(this.loading);
    if (!response?.Success) {
      this.dialogService.showDialog(response?.Message!);
      return;
    }

    this.handleConnectionResult.emit(response.Connections!);

  }

  invertDepartureArrival() {
    if (this.connectionReq.DepartArretID && this.connectionReq.ArrivalArretID) {
      const temp = this.connectionReq.ArrivalArretID;
      this.connectionReq.ArrivalArretID = this.connectionReq.DepartArretID;
      this.connectionReq.DepartArretID = temp;
    }
  }

  handleCitySelected(citySelectedId: number, req: number) {
    req = citySelectedId;
  }

  getCityFromCityID(id: any) {
    return this.citiesValues.find(x => x.ID === id) as CityDto;
  }

}
