import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ConnectionRequest } from '../../../services/api-client.generated';

@Component({
    selector: 'app-filter-box',
    templateUrl: './filter-box.component.html',
    encapsulation: ViewEncapsulation.None,
})

export class FilterBoxComponent {

    @Input()
    request: ConnectionRequest = {};

    @Output()
    requestChange = new EventEmitter<ConnectionRequest>();

    constructor() {

    }


    onRequestChange() {
        this.requestChange.emit(this.request);
    }
}