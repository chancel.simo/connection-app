import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { FilterBoxComponent } from "./filter-box.component";
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from "@angular/forms";


@NgModule({
    declarations: [FilterBoxComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCheckboxModule,
        FormsModule
    ],
    exports: [FilterBoxComponent]
})
export class FilterBoxModule { }