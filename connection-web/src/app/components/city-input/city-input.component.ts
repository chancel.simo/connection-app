import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { CityDto } from '../../../services/api-client.generated';

@Component({
  selector: 'app-city-input',
  templateUrl: './city-input.component.html',
  styleUrls: ['./city-input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CityInputComponent implements OnInit {
  loading: boolean = false;

  private _cities: CityDto[] = [];

  get cities(): CityDto[] {
    return this._cities;
  }

  @Input() set cities(v: CityDto[]) {
    this._cities = v;
  }


  private _citySelectedId!: number;

  get citySelectedId(): number {
    return this._citySelectedId;
  }

  @Input() set
    citySelectedId(v: number) {
    this._citySelectedId = v;
    this.handleCitySelected(v);
  }

  @Input()
  placeHolder: string = "Ville";

  innerValue!: string;

  citiesFiltered: CityDto[] = [];

  @Output()
  citySelectedChange = new EventEmitter<number>();

  constructor() {

  }

  ngOnInit() {
  }

  handleCityChange() {
    if (!this.innerValue || this.innerValue.length < 2)
      return;

    this.citiesFiltered = this._cities.filter(x => x.CityValue!.Label!.toLowerCase().match(this.innerValue.toLowerCase())).slice(0, 5);
  }

  handleCitySelected(cityId: number) {
    const city = this.cities.find(x => x.ID === cityId) as CityDto;

    if (!city)
      return;

    this.innerValue = city.CityValue!.Label as string;
    this.citiesFiltered = [];
    this.citySelectedChange.emit(city.ID);
  }

  clearInput() {
    this.innerValue = '';
    this.citiesFiltered = [];
  }

  onKeydown(event: any) {
    console.log("🚀 ~ onKeydown ~ event", event);

  }

}
