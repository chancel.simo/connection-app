import { ComponentType } from "@angular/cdk/portal";
import { Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DialogComponent } from "./dialog.component";

export interface IDialogOptions {
    content?: string;
    title?: string;
}

export interface ICustomDialogOption<T> extends MatDialogConfig {
    component: ComponentType<T>;
    data?: any;
}

@Injectable({
    providedIn: 'root',
})
export class DialogService {

    constructor(
        private _dialog: MatDialog,
        private _snackBar: MatSnackBar
    ) { }

    showDialog(message: string): MatDialogRef<DialogComponent> {
        return this._dialog.open(DialogComponent, {
            data: message,
            minWidth: 300,
            minHeight: 200
        });
    }

    showDialogAwaitable<T>(opts: ICustomDialogOption<T>): MatDialogRef<T> {
        return this._dialog.open<T>(opts.component, {
            data: opts.data,
            minHeight: opts.minHeight,
            minWidth: opts.minWidth,
            disableClose: opts.disableClose,
        });
    }

    showSnackBar(message: string) {
        this._snackBar.open(message, undefined, {
            duration: 2000
        });
    }
}