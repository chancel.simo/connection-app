using ConnectionAPI.DAL;
using System.Diagnostics;
using ConnectionAPI.DAL.Models;
using AutoMapper;

namespace ConnectionAPI.Services
{
    public class DatabaseService
    {
        public static void UpdateDatabaseIfNeeded(DataContext context, IMapper mapper, HttpClient httpClient)
        {
            try
            {
                context.CreateInitialData(mapper, httpClient).Wait();
                Console.WriteLine("Database initiated correctly.");

            }
            catch (System.Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
    }
}