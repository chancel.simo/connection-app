using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using ConnectionAPI.Config;
using ConnectionAPI.DAL;
using AutoMapper;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Services
{
    public class GlobalService
    {
        public static bool IsCookieDefined(string CookieKey)
        {
            var Cookie = Server.CurrentContext.GetCookie(CookieKey);
            return Cookie != null && !string.IsNullOrWhiteSpace(Server.CurrentContext.GetCookie(CookieKey));
        }

        public static void InitApp(TravelSessionManager travelSessionManager, IHttpContext httpContext, IWebHostEnvironment env)
        {
            Server.CurrentContext = httpContext;
            Server.SiteBaseDirectory = env.ContentRootPath;
            MySession.Init(travelSessionManager);
        }

        public static void InitSession(DataContext context, IMapper mapper, IConfiguration configuration)
        {
            var SessionInitialized = MySession.Instance?.SessionInitialized;

            if (SessionInitialized.HasValue && !SessionInitialized.Value)
            {
                MySession.Instance.SessionId = Guid.NewGuid().ToString();
                var IsCookieDefined = GlobalService.IsCookieDefined(AppSettings.TravelCookieKey);

                if (MySession.Instance.CurrentUser == null && IsCookieDefined)
                {
                    var refreshTokenCookie = Server.CurrentContext.GetCookie(AppSettings.TravelCookieKey);
                    AutoConnectUser(context, mapper, configuration, refreshTokenCookie);
                }

                MySession.Instance.SessionInitialized = true;
            }
            else
            {
                if (!SessionInitialized.HasValue)
                    Console.WriteLine("Unable to init Session : MySession.Instance is null");
            }
        }

        public static GenericResponse AutoConnectUser(DataContext context, IMapper mapper, IConfiguration configuration, string refreshTokenCookie)
        {
            var response = new GenericResponse();
            try
            {
                if (refreshTokenCookie == null || string.IsNullOrWhiteSpace(refreshTokenCookie))
                {
                    throw new Exception("Unable to init Session : no refresh token");
                }

                var user = UsersService.GetUserByRefreshToken(context, refreshTokenCookie, mapper);
                if (user != null)
                {
                    //Check validity of refresh token
                    if (user.AuthenticationTokens == null || user.AuthenticationTokens.Any(x => x.RefreshTokenExpiryTime < DateTime.Now))
                    {
                        throw new Exception("Unable to init Session : refresh token has expired");
                    }

                    response = UsersService.Connect(context, mapper, configuration, user, true, false);
                }
            }
            catch (Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }
            return response;
        }

        /* public static async Task SetFolderWriteAccessAsync(string folder)
        {
            if (string.IsNullOrWhiteSpace(folder) || folder.Length < 5 || folder == "/")
                return;
            await Nextalys.Helpers.Main.MainHelpers.RunCommand("chmod 777 " + folder);
        } */
        public static async Task CreateDirectoryIfNeededWithWriteAccess(string folder)
        {
            if (string.IsNullOrWhiteSpace(folder) || Directory.Exists(folder))
                return;
            Directory.CreateDirectory(folder);
            // await SetFolderWriteAccessAsync(folder);
        }

        /* public static async Task RunCommand(string command)
        {
            var process = new Process();

            process.StartInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = command,
                WindowStyle = ProcessWindowStyle.Hidden,
            };
        } */
    }
}