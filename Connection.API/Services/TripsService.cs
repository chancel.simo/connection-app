using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
namespace ConnectionAPI.Services
{
    public class TripsService
    {
        public static TripsResponse GetTripsList(DataContext ctx, IMapper mapper, TripRequest request)
        {
            var response = new TripsResponse();
            try
            {

                IQueryable<Trip> tripQueryable = ctx.TripsReadOnly
                                                    .Include(x => x.Connection)
                                                    .ThenInclude(x => x.Agent)
                                                    .Include(x => x.ListArrets)
                                                    .ThenInclude(x => x.City)
                                                    .ThenInclude(x => x.CityValue)
                                                    .Include(x => x.Frequence)
                                                    .Include(x => x.Auto)
                                                    .Include(x => x.PriceTrips);
                // TODO
                /* if (!MySession.Instance.CurrentUser.IsSuperAdmin)
                {
                    var currentUserAgency = ctx.AgenciesReadOnly.Include(x => x.Employees)
                                                                .FirstOrDefault(x => x.Employees.Any(y => y.ID == MySession.Instance.CurrentUser.ID));

                    tripQueryable = tripQueryable.Where(x => x.Connection.Agent.AgencyID == currentUserAgency.ID);
                } */

                if (tripQueryable == null)
                    throw new Exception("error occured when loading trip");

                if (request.ConnectionID != null)
                {
                    tripQueryable = tripQueryable.Where(x => x.ConnectionID == request.ConnectionID);
                }

                var tripsResponse = tripQueryable.ToList();
                if (tripsResponse == null)
                    throw new Exception("error when loading trips");

                response.TripsList = new List<TripDto>();
                mapper.Map<List<Trip>, List<TripDto>>(tripsResponse, response.TripsList);
                response.Success = true;
            }
            catch (System.Exception ex)
            {
                response.HandleResponse(ex.Message);
            }
            return response;
        }

        public static TripResponse SaveTrip(DataContext ctx, IMapper mapper, TripDto trip, bool notSave = false)
        {
            var response = new TripResponse();
            try
            {
                var tripEntity = new Trip();

                mapper.Map<TripDto, Trip>(trip, tripEntity);

                if (tripEntity.ID > 0)
                    ctx.Trips.Update(tripEntity);
                else
                    tripEntity = ctx.Trips.Add(tripEntity).Entity;

                if (!notSave)
                    ctx.SaveChanges();

                response.Trip = new TripDto();
                mapper.Map<Trip, TripDto>(tripEntity, response.Trip);


                response.Success = true;
            }
            catch (System.Exception ex)
            {
                response.HandleResponse(ex.Message);
            }
            return response;
        }
    }
}