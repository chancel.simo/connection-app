using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.Services
{

    public interface TravelSessionManager
    {
        ITravelSession Instance { get; }

    }
    public interface ITravelSession
    {
        string SessionId { get; set; }
        UserDto CurrentUser { get; set; }
        //   UserDto ConnectAsRequester { get; set; }
        bool SessionInitialized { get; set; }
    }

    public class MySession
    {
        /// <summary>
        /// Cadenas du singleton
        /// </summary>
        private static readonly object padlock = new object();

        /// <summary>
        /// Prevents a default instance of the <see cref="MySession" /> class from being created
        /// </summary>
        private MySession()
        {
        }

        public static ITravelSession Instance
        {
            get
            {
                return Manager?.Instance;
            }
        }
        public static TravelSessionManager Manager;

        public static void Init(TravelSessionManager manager)
        {
            Manager = manager;
        }
    }
}