using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ConnectionAPI.Config;


namespace ConnectionAPI.Services
{
    public class AgenciesService
    {
        public static async Task<AgenciesResponse> GetAgencies(DataContext context, IMapper mapper)
        {
            var response = new AgenciesResponse();
            try
            {
                var agenciesRespons = context.AgenciesReadOnly
                                                        .Include(x => x.Frequences)
                                                        .Include(x => x.Employees)
                                                        .ThenInclude(x => x.UserRoles)
                                                        .ThenInclude(x => x.Role)
                                                        .Include(x => x.Vehicles)
                                                        .ToList();

                response.Agencies = new List<AgencyDto>();
                mapper.Map(agenciesRespons, response.Agencies);

                response.Success = true;
            }
            catch (System.Exception ex)
            {
                response.HandleResponse(ex.Message);
            }

            return response;
        }
        public static async Task<AgencyResponse> GetConnectedUserAgency(DataContext context, IMapper mapper)
        {
            return await GetAgency(context, mapper, MySession.Instance.CurrentUser.AgencyID);
        }

        public static async Task<AgencyResponse> GetAgency(DataContext context, IMapper mapper, int AgencyID)
        {
            var response = new AgencyResponse();
            try
            {

                var agencyResponse = await context.AgenciesReadOnly
                                                                .Include(x => x.Frequences)
                                                                .Include(x => x.Employees)
                                                                .ThenInclude(x => x.UserRoles)
                                                                .ThenInclude(x => x.Role)
                                                                .Include(x => x.Vehicles)
                                                                .FirstOrDefaultAsync(x => x.ID == AgencyID);



                if (agencyResponse == null)
                    throw new Exception("no agency for this user");

                agencyResponse.Employees = agencyResponse.Employees.Where(x => x.ID != MySession.Instance.CurrentUser.ID).ToList();

                response.Agency = new AgencyDto();
                mapper.Map<Agency, AgencyDto>(agencyResponse, response.Agency);

                response.Success = true;
            }
            catch (System.Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }
            return response;
        }

        public static async Task<AgencyResponse> SaveAgency(DataContext context, IMapper mapper, IConfiguration configuration, AgencyDto agency)
        {
            var response = new AgencyResponse();
            try
            {
                if (agency == null)
                    throw new Exception("aucune agence saisi");

                var agencyEntity = new Agency();


                mapper.Map<AgencyDto, Agency>(agency, agencyEntity);

                if (!string.IsNullOrEmpty(agency.LogoPath) && !string.IsNullOrEmpty(agency.LogoName))
                {
                    var sourceFile = Path.Combine(Server.SiteBaseDirectory, "Upload", "TempUpload");

                    var destFile = Path.Combine(Server.SiteBaseDirectory, "Upload", "Public");
                    var ApiBaseUrl = configuration.GetSection("ApiBaseUrl").Value;

                    await GlobalService.CreateDirectoryIfNeededWithWriteAccess(destFile);

                    var FilePath = Path.Combine(sourceFile, agency.LogoName);
                    var FileDest = Path.Combine(destFile, agency.LogoName);
                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Copy(FilePath, FileDest, true);
                        agencyEntity.LogoPath = ApiBaseUrl + "/files/Public/" + agency.LogoName;
                        agencyEntity.LogoName = agency.LogoName;
                        System.IO.File.Delete(FilePath);
                    }
                }

                var doNotUpdatePasswordList = new List<int>();
                var roles = await context.RolesReadOnly
                                                .Include(x => x.UserRoles)
                                                .ToListAsync();

                foreach (var userEntity in agencyEntity.Employees!)
                {
                    if (userEntity.ID > 0)
                    {
                        var userDto = agency.Employees.FirstOrDefault(x => x.Email == userEntity.Email);

                        if (!string.IsNullOrWhiteSpace(userDto.Password))
                        {
                            UsersService.CreatePasswordHash(userDto.Password, out byte[] passwordHash, out byte[] passwordSalt);
                            userEntity.PasswordHash = passwordHash;
                            userEntity.PasswordSalt = passwordSalt;
                            userDto.Password = null;
                        }
                        else
                        {
                            doNotUpdatePasswordList.Add(userEntity.ID);
                        }
                    }
                    else
                    {
                        userEntity.LoginToken = Guid.NewGuid().ToString();
                        var crmClientBaseUrl = configuration.GetSection("CRMClientBaseUrl").Value;

                        var mailData = new MailData
                        {
                            To = userEntity.Email,
                            From = "no-reply@connection-trip.fr",
                            CC = "test@test.fr",
                            Subject = "Inscription sur Connection.",
                            MailBody = "Bonjour, vous recevez ce mail suite à votre ajout au sein de l'agence " + agencyEntity.Name +
                                        ". En cliquant sur le lien ci-dessous, vous serez amené à choisir un mot de passe pour acceder a votre compte personnel.<br/><br/>" +
                                        crmClientBaseUrl + "/register-with-guid?token=" + userEntity.LoginToken,
                        };

                        var mailResponse = await MailService.SendMail(mailData);
                    }



                    foreach (var userRole in userEntity.UserRoles)
                    {
                        if (userRole.Role != null && (userRole.RoleID == null || userRole.RoleID == 0))
                        {
                            var roleToAssociate = roles.FirstOrDefault(x => x.RoleCode == userRole.Role.RoleCode);
                            userRole.RoleID = roleToAssociate!.ID;
                            //  userRole.Role = null;   
                        }
                    }

                    foreach (var role in roles)
                    {
                        var item = userEntity.UserRoles.FirstOrDefault(x => role.UserRoles != null && role.UserRoles.Any(ur => ur.Role.RoleCode == x.Role.RoleCode));
                        if (item != null)
                        {
                            if (item.ID > 0)
                            {
                                context.Entry(item).State = EntityState.Deleted;
                            }
                        }
                    }



                }

                var agencyResponse = new Agency();

                if (agency.ID > 0)
                {
                    agencyResponse = context.Agencies.Update(agencyEntity).Entity;
                    foreach (var item in agencyEntity.Employees)
                    {
                        if (item.ID > 0 && doNotUpdatePasswordList.Any(x => x == item.ID))
                        {
                            context.Entry(item).Property(x => x.PasswordHash).IsModified = false;
                            context.Entry(item).Property(x => x.PasswordSalt).IsModified = false;
                        }
                    }
                }
                else
                    agencyResponse = context.Agencies.Add(agencyEntity).Entity;

                context.SaveChanges();

                response.Agency = new AgencyDto();
                mapper.Map<Agency, AgencyDto>(agencyResponse, response.Agency);
                response.Success = true;
            }
            catch (System.Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }

            return response;
        }
    }
}