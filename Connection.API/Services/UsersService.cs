using ConnectionAPI.DAL.DTO;
using System.Security.Cryptography;
using System.Security.Claims;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using ConnectionAPI.Config;
using Newtonsoft.Json;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Web;
using Microsoft.AspNetCore.Authorization;

namespace ConnectionAPI.Services
{
    public class UsersService
    {
        public static User user = new User();

        public async static Task<UserResponse> RegisterAsync(DataContext context, IMapper mapper, UserDto request)
        {
            UserResponse response = new UserResponse();
            try
            {
                CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);

                if (request == null)
                    throw new Exception("no request send");

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.Email = request.Email;
                user.LastName = request.LastName;
                user.FirstName = request.FirstName;
                user.UserRoles = request.UserRoles?.Select(x => new UserRole
                {
                    ID = x.ID,
                }).ToList();

                var createUserResponse = await context.Users.AddAsync(user);
                await context.SaveChangesAsync();

                response.User = new UserDto();

                mapper.Map<User, UserDto>(createUserResponse.Entity, response.User);
                MySession.Instance.CurrentUser = response.User;
                response.Success = true;
            }
            catch (Exception err)
            {
                response.HandleResponse(err.Message);
            }
            return response;
        }

        public static async Task<UserResponse> Login(DataContext context, IMapper mapper, IConfiguration configuration, HttpContext httpContext, LoginRequest request)
        {
            UserResponse response = new UserResponse();
            try
            {
                var userResponse = await context.Users.Where(x => x.Email == request.Email)
                                                         .Include(x => x.Agency)
                                                         .Include(x => x.UserRoles)
                                                         .ThenInclude(x => x.Role)
                                                         .FirstOrDefaultAsync();

                if (userResponse == null)
                {
                    throw new Exception("user not found");
                }
                else
                {
                    if (!VerifyPasswordHash(request.Password, userResponse.PasswordHash, userResponse.PasswordSalt))
                    {
                        throw new Exception("Password not correct");
                    }
                }

                response = Connect(context, mapper, configuration, userResponse, request.RememberMe, true);

            }
            catch (Exception err)
            {
                response.HandleResponse(err.Message);
            }
            return response;
        }

        public static RefreshToken GenerateRefreshToken()
        {
            return new RefreshToken
            {
                Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
                Expires = DateTime.Now.AddDays(7),
                Created = DateTime.Now,
            };

        }

        public static void SetRefreshToken(RefreshToken newRefreshToken, User userToRefresh)
        {
            if (userToRefresh.AuthenticationTokens == null)
                userToRefresh.AuthenticationTokens = new List<AuthenticationToken>();

            if (GlobalService.IsCookieDefined(newRefreshToken.Token))
            {
                var oldToken = Server.CurrentContext.GetCookie(AppSettings.TravelCookieKey);
                if (userToRefresh.AuthenticationTokens.Any(x => x.Token == oldToken))
                    userToRefresh.AuthenticationTokens.RemoveAll(x => x.Token == oldToken);
            }
            userToRefresh.AuthenticationTokens.Add(
                new AuthenticationToken
                {
                    UserID = userToRefresh.ID,
                    CreationDate = DateTime.Now,
                    Token = newRefreshToken.Token,
                    RefreshTokenExpiryTime = DateTime.Now.AddDays(7),
                }
            );

            Server.CurrentContext.AddCookie(AppSettings.TravelCookieKey, newRefreshToken.Token, newRefreshToken.Expires);
        }

        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }

        }

        public static string CreateToken(User user, IConfiguration configuration)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim("Email", user.Email),
                new Claim("FullName", user.LastName + " " + user.FirstName),
                new Claim("ID", user.ID.ToString()),
            };

            if (user.UserRoles != null)
            {
                foreach (var userRole in user.UserRoles)
                {
                    claims.Add(new Claim("Role", userRole.Role.RoleCode));
                }
            }

            if (user.Agency != null && !string.IsNullOrEmpty(user.Agency.LogoPath))
            {
                claims.Add(new Claim("AgencyLogo", user.Agency.LogoPath));
            }

            var tokenSecretKey = configuration.GetSection("TokenSecretKey").Value;

            var secretKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(tokenSecretKey));
            var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddHours(12),
                signingCredentials: credentials
            );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        public static UserDto? GetCurrentUser(HttpContext httpContext)
        {
            var identity = httpContext.User.Identity as ClaimsIdentity;

            if (identity != null)
            {
                var userClaims = identity.Claims;

                if (userClaims == null)
                    return null;

                var userDto = new UserDto
                {
                    Email = userClaims.FirstOrDefault(x => x.Type == "Email")?.Value!,
                    FullName = userClaims.FirstOrDefault(x => x.Type == "FullName")?.Value!,
                    ID = Int32.Parse(userClaims.FirstOrDefault(x => x.Type == "ID")?.Value!),
                    UserRoles = new List<UserRoleDto>
                    {
                        new UserRoleDto
                        {
                            Role = new RoleDto
                            {
                                RoleCode = userClaims.FirstOrDefault(x => x.Type == "Role")?.Value,
                            }
                        }
                    },
                };

                return userDto;
            }

            return null;
        }

        public static async Task<UserResponse> GetUser(DataContext context, IMapper mapper, int ID)
        {
            var response = new UserResponse();
            try
            {
                var userResponse = await context.UsersReadOnly.Where(x => x.ID == ID)
                                            .Include(x => x.Agency)
                                            .ThenInclude(x => x.Vehicles)
                                            .Include(x => x.Agency)
                                            .ThenInclude(x => x.Frequences)
                                            .Include(x => x.UserRoles)
                                            .ThenInclude(x => x.Role)
                                            .FirstOrDefaultAsync();

                if (userResponse == null)
                    throw new Exception("une erreur est survenue lors de la recuperation du user");

                response.User = new UserDto();
                mapper.Map<User, UserDto>(userResponse, response.User);
                response.Success = true;
            }
            catch (System.Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }
            return response;
        }

        public static async Task<UsersResponse> GetUsers(DataContext context, IMapper mapper, GetUserRequest request)
        {
            var response = new UsersResponse();
            try
            {
                IQueryable<User> userQueryable = context.UsersReadOnly.Include(x => x.UserRoles)
                                                                     .ThenInclude(x => x.Role);
                if (request != null)
                {
                    if (request.AgencyID > 0)
                    {
                        userQueryable = userQueryable.Where(x => x.AgencyID == request.AgencyID);
                    }

                    /* if (MySession.Instance.CurrentUser.IsSuperAdmin)
                    {
                        userQueryable = userQueryable.Where(x => x.ID != MySession.Instance.CurrentUser.ID &&
                                                                             x.UserRoles.Any(y => y.Role.RoleCode == Constants.AdminRoleName ||
                                                                                      y.Role.RoleCode == Constants.SuperAdminRoleName));
                    } */
                    else if (request.GetOnlyEmployee)
                    {
                        userQueryable = userQueryable.Where(x => x.UserRoles.Any(y => y.Role.RoleCode == Constants.EmployeeRoleName));
                    }

                    if (request.GetOnlyAdmin)
                    {
                        userQueryable = userQueryable.Where(x => x.UserRoles.Any(y => y.Role.RoleCode == Constants.AdminRoleName));
                    }
                }


                var users = await userQueryable.ToListAsync();

                if (users == null)
                    throw new Exception("une erreur est survenue lors de la recuperation du user");

                response.Users = new List<UserDto>();
                mapper.Map<List<User>, List<UserDto>>(users, response.Users);
                response.Success = true;
            }
            catch (System.Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }
            return response;
        }

        public static async Task<UserResponse> SaveUser(DataContext context, IMapper mapper, UserDto user)
        {
            var userResponse = new UserResponse();
            try
            {
                if (user.ID > 0)
                {
                    var userEntity = await context.Users.FirstOrDefaultAsync(x => x.ID == user.ID);
                    if (userEntity == null)
                        throw new Exception("Error user");

                    if (user.Agency?.Vehicles != null)
                    {
                        foreach (var item in user.Agency.Vehicles)
                        {
                            if (string.IsNullOrWhiteSpace(item.Ref))
                                item.Ref = (user.Agency.Name + "_" + item.Matricule).ToLower();
                        }
                    }

                    mapper.Map(user, userEntity);

                    if (!string.IsNullOrWhiteSpace(user.Password))
                    {
                        CreatePasswordHash(user.Password, out byte[] passwordHash, out byte[] passwordSalt);
                        userEntity.PasswordHash = passwordHash;
                        userEntity.PasswordSalt = passwordSalt;
                    }


                    context.Users.Update(userEntity);
                    await context.SaveChangesAsync();

                }
                else
                {
                    userResponse = await RegisterAsync(context, mapper, user);
                }

                userResponse.Success = true;
            }
            catch (Exception exception)
            {
                userResponse.HandleResponse(exception.Message);
            }
            return userResponse;
        }

        public static async Task<FrequencesResponse> SaveFrequences(DataContext context, IMapper mapper, FrequenceRequest request)
        {
            var frequencesResponse = new FrequencesResponse();
            try
            {
                foreach (var frequence in request.Frequences)
                {
                    var response = new Frequence();
                    response = mapper.Map<FrequenceDto, Frequence>(frequence, response);
                    if (frequence.ID > 0)
                    {
                        context.Frequences.Update(response);
                    }
                    else
                    {
                        await context.Frequences.AddAsync(response);
                    }


                    await context.SaveChangesAsync();

                    if (response == null)
                        throw new Exception("une erreur est survenue lors de la sauvegarde");

                    if (frequencesResponse.Frequences == null || frequencesResponse.Frequences?.Count == 0)
                        frequencesResponse.Frequences = new List<FrequenceDto>();

                    FrequenceDto responseDto = mapper.Map<Frequence, FrequenceDto>(response, frequence);

                    frequencesResponse.Frequences.Add(responseDto);
                }


                frequencesResponse.Success = true;
            }
            catch (Exception exception)
            {
                frequencesResponse.HandleResponse(exception.Message);
            }
            return frequencesResponse;
        }

        public static User GetUserByRefreshToken(DataContext context, string token, IMapper mapper)
        {
            var user = new User();
            try
            {
                var userResponse = context.UsersReadOnly.Include(x => x.UserRoles)
                                                        .ThenInclude(x => x.Role)
                                                        .Include(x => x.AuthenticationTokens)
                                                        .FirstOrDefault(x => x.AuthenticationTokens != null &&
                                                                             x.AuthenticationTokens.Any(x => x.Token == token));

                if (userResponse == null)
                    throw new Exception("No user match refresh token");

                user = userResponse;
            }
            catch (System.Exception Exception)
            {
                Console.WriteLine("An error occured while retrieving user by refresh token");
            }

            return user;
        }

        public static UserResponse Connect(DataContext context, IMapper mapper, IConfiguration configuration, User user, bool RememberMe, bool GenerateNewToken)
        {
            var response = new UserResponse();
            try
            {
                if (user == null)
                {
                    response.Message = "Un utilisateur doit être spécifié pour la connexion...";
                    return response;
                }

                if (RememberMe && GenerateNewToken)
                {
                    var refreshToken = GenerateRefreshToken();
                    SetRefreshToken(refreshToken, user);

                    context.SaveChanges();
                }
                else if (!RememberMe)
                {
                    Server.CurrentContext.RemoveCookie(AppSettings.TravelCookieKey);
                }

                response.User = new UserDto();

                mapper.Map(user, response.User);
                response.User.UserRoles.ForEach(x =>
                {
                    x.User = null;
                    x.Role.UserRoles = null;
                });

                response.User.AuthenticationTokens.ForEach(x =>
                {
                    x.User = null;
                });

                response.User.Agency.Employees = null;

                MySession.Instance.CurrentUser = response.User;

                response.User.AccessToken = response.access_token = CreateToken(user, configuration);

                response.Success = true;
            }
            catch (Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }
            return response;
        }

        public static async Task<GenericResponse> Logout(DataContext context)
        {
            var response = new GenericResponse();
            try
            {
                if (GlobalService.IsCookieDefined(AppSettings.TravelCookieKey))
                {
                    var user = await context.Users.Include(x => x.AuthenticationTokens).FirstOrDefaultAsync(x => x.ID == MySession.Instance.CurrentUser.ID);
                    if (user != null && user.AuthenticationTokens != null)
                    {
                        user.AuthenticationTokens.RemoveAll(x => x.Token == Server.CurrentContext.GetCookie(AppSettings.TravelCookieKey));

                        await context.SaveChangesAsync();
                    }
                }

                MySession.Instance.CurrentUser = null;
                Server.CurrentContext.RemoveCookie(AppSettings.TravelCookieKey);
                response.Success = true;
            }
            catch (Exception Exception)
            {
                response.HandleResponse(Exception.Message);
            }
            return response;
        }


        public static async Task<UserResponse> GetUserByToken(DataContext context, IMapper mapper, string loginToken)
        {
            var response = new UserResponse();
            try
            {
                var userResponse = await context.UsersReadOnly.FirstOrDefaultAsync(x => x.LoginToken == loginToken);
                if (userResponse == null)
                    throw new Exception("utilisateur incorrect");

                else if (userResponse.IsActive)
                    throw new Exception("Ce lien à déja été utilisé");

                response.User = new UserDto();
                mapper.Map(userResponse, response.User);

                response.Success = true;
            }
            catch (System.Exception ex)
            {
                response.HandleResponse(ex.Message);
            }
            return response;
        }

        public static async Task<GenericResponse> RegisterWithToken(DataContext context, UserDto user)
        {
            var response = new GenericResponse();
            try
            {
                var userResponse = await context.Users.FirstOrDefaultAsync(x => x.ID == user.ID);

                if (userResponse == null)
                    throw new Exception("l'utilisateur saisi n'existe pas");

                if (string.IsNullOrWhiteSpace(user.Password))
                    throw new Exception("le mot de passe n'a pas été saisi");

                CreatePasswordHash(user.Password, out byte[] passwordHash, out byte[] passwordSalt);

                userResponse.PasswordHash = passwordHash;
                userResponse.PasswordSalt = passwordSalt;

                userResponse.IsActive = true;

                context.Users.Update(userResponse);

                await context.SaveChangesAsync();

                response.Success = true;
            }
            catch (System.Exception ex)
            {
                response.HandleResponse(ex.Message);
            }
            return response;
        }
    }
}