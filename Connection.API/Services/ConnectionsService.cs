using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ConnectionAPI.Services;

public class ConnectionService
{
    public static ConnectionsResponse GetConnectionList(DataContext context, IMapper mapper, ConnectionRequest request)
    {
        ConnectionsResponse response = new ConnectionsResponse();
        try
        {
            IQueryable<Connection> connectionQueryable;

            connectionQueryable = context.Connections
                                                .Include(x => x.Agent)
                                                .ThenInclude(x => x.Agency)
                                                .Include(x => x.Trips)
                                                .ThenInclude(x => x.PriceTrips)
                                                .Include(x => x.Trips)
                                                .ThenInclude(x => x.ListArrets)
                                                .ThenInclude(x => x.City)
                                                .ThenInclude(x => x.CityValue);

            if (request.EmployeeID != null)
            {
                connectionQueryable = connectionQueryable.Where(x => x.AgentID == request.EmployeeID);
            }

            if (request.DepartArretID != null && request.ArrivalArretID != null)
            {
                var now = DateTime.Now;

                var departArretList = context.ArretsReadOnly.Where(x =>
                                                                        x.CityID == request.DepartArretID
                                                                        //  x.DepartureTime!.Value.TimeOfDay >= now.TimeOfDay
                                                                        ).AsQueryable();



                var arrivalArretList = context.ArretsReadOnly.Where(x =>
                                                                        x.CityID == request.ArrivalArretID &&
                                                                        departArretList.Any(o => o.Order < x.Order)
                                                                        // x.ArrivalTime.Value.TimeOfDay > now.TimeOfDay
                                                                        ).AsQueryable();



                var tripsResponse = context.TripsReadOnly.Where(x =>
                                                                    departArretList.Any(d => d.TripID == x.ID) &&
                                                                    arrivalArretList.Any(a => a.TripID == x.ID))
                                                                    .Include(x => x.PriceTrips)
                                                                    .AsQueryable();

                connectionQueryable = connectionQueryable.Where(x => tripsResponse.Any(t => t.ConnectionID == x.ID));
            }

            if (request.StartDate != null)
            {

                var frequenceResponse = context.FrequencesReadOnly.Where(x => x.StartDate <= request.StartDate && request.StartDate <= x.EndDate &&
                             (
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Sunday && x.Dim.HasValue && x.Dim.Value) ||
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Monday && x.Lun.HasValue && x.Lun.Value) ||
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Tuesday && x.Mar.HasValue && x.Mar.Value) ||
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Wednesday && x.Mer.HasValue && x.Mer.Value) ||
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Thursday && x.Jeu.HasValue && x.Jeu.Value) ||
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Friday && x.Ven.HasValue && x.Ven.Value) ||
                                 (request.StartDate.Value.DayOfWeek == DayOfWeek.Saturday && x.Sam.HasValue && x.Sam.Value)
                             )).AsQueryable();


                connectionQueryable = connectionQueryable.Where(x => x.Trips != null &&
                                                                     x.Trips.Any(y => frequenceResponse != null &&
                                                                                      frequenceResponse.Any(f => f.ID == y.FrequenceID)));
            }

            if (request.EarliestDeparture.HasValue && request.EarliestDeparture.Value && connectionQueryable.Count() > 0)
            {
                foreach (var item in connectionQueryable)
                {
                    if (item.Trips != null && item.Trips.Count() > 0)
                        item.Trips.OrderBy(x => x.DepartureTime);
                }
            }

            var connectionsResponse = connectionQueryable.ToList();


            if (connectionsResponse != null)
            {
                response.Connections = new List<ConnectionDto>();
                mapper.Map<List<Connection>, List<ConnectionDto>>(connectionsResponse, response.Connections);

                if (response.Connections.Count > 0)
                {

                    foreach (var connection in response.Connections)
                    {
                        foreach (var trip in connection.Trips!)
                        {
                            var departArret = trip.ListArrets!.FirstOrDefault(x => x.CityID == request.DepartArretID);
                            var arrivalArret = trip.ListArrets!.FirstOrDefault(x => x.CityID == request.ArrivalArretID);

                            if (departArret != null && departArret.Order != trip.DepartureArret!.Order)
                                trip.SetRequestDepartureArretID(departArret.ID!.Value);

                            if (arrivalArret != null && arrivalArret.Order != trip.TerminusArret!.Order)
                                trip.SetRequestArrivalArret(arrivalArret.ID!.Value);

                        }
                    }
                }

            }

            response.Success = true;
        }
        catch (System.Exception err)
        {
            response.HandleResponse(err.Message);
        }
        return response;
    }

    public static ConnectionResponse CreateConnection(DataContext context, IMapper mapper, ConnectionDto connection)
    {
        ConnectionResponse response = new ConnectionResponse();
        response.Connection = new ConnectionDto();
        try
        {
            if (connection == null || connection.Trips == null)
                throw new Exception("Aucun voyage saisi.");


            foreach (var trip in connection.Trips)
            {
                if (trip.ListArrets?.Count > 0)
                {
                    trip.ListArrets.OrderBy(a => a.DepartureTime != null ? a.DepartureTime : a.ArrivalTime);
                    trip.Frequence = null;
                    trip.Auto = null;

                    foreach (var item in trip.ListArrets.Select((arret, i) => new { i, arret }))
                    {
                        if (item.arret != null)
                            item.arret.City = null;

                        if (item.arret != null && item.arret.Order == 0)
                        {
                            item.arret.Order = item.i + 1;
                        }

                        /*  if (item.arret != null && (item.arret.TripID == 0 || item.arret.TripID == null))
                             item.arret.City = null; */
                    }
                }

                if (trip.PriceTrips != null)
                {
                    foreach (var priceTrip in trip.PriceTrips)
                    {
                        priceTrip.ArrivalArret = null;
                        priceTrip.DepartArret = null;
                    }

                }

                if (trip.ConnectionID != 0 && connection.ID > 0)
                    trip.ConnectionID = connection.ID;

            }

            var connectionEntity = new Connection();


            mapper.Map<ConnectionDto, Connection>(connection, connectionEntity);

            if (connection.ID > 0)
            {
                context.Connections.Update(connectionEntity);
            }
            else
                context.Connections.Add(connectionEntity);

            context.SaveChanges();

            var responseConnection = GetConnection(context, mapper, connectionEntity.ID);
            if (responseConnection.Success)
                response.Connection = responseConnection.Connection;

            response.Success = true;
        }
        catch (System.Exception exception)
        {
            response.HandleResponse(exception.Message);
        }
        return response;
    }

    public static ConnectionResponse GetConnection(DataContext context, IMapper mapper, int ConnectionID)
    {
        var response = new ConnectionResponse();
        try
        {
            var connectionResponse = context.ConnectionsReadOnly.Where(x => x.ID == ConnectionID)
                                                                    .Include(x => x.Agent)
                                                                    .Include(x => x.Auto)
                                                                    .Include(x => x.Trips)
                                                                    .ThenInclude(x => x.Frequence)
                                                                    .Include(x => x.Trips)
                                                                    .ThenInclude(x => x.ListArrets)
                                                                    .ThenInclude(x => x.City)
                                                                    .ThenInclude(x => x.CityValue)
                                                                    .Include(x => x.Trips)
                                                                    .ThenInclude(x => x.PriceTrips)
                                                                    .ThenInclude(x => x.ArrivalArret)
                                                                    .ThenInclude(x => x.City)
                                                                    .ThenInclude(x => x.CityValue)
                                                                    .Include(x => x.Trips)
                                                                    .ThenInclude(x => x.PriceTrips)
                                                                    .ThenInclude(x => x.DepartArret)
                                                                    .ThenInclude(x => x.City)
                                                                    .ThenInclude(x => x.CityValue)
                                                                    .FirstOrDefault();

            if (connectionResponse != null)
            {
                response.Connection = new ConnectionDto();
                mapper.Map<Connection, ConnectionDto>(connectionResponse, response.Connection);
            }

            response.Success = true;
        }
        catch (Exception exception)
        {
            response.HandleResponse(exception.Message);
        }

        return response;
    }
}
