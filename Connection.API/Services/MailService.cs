using ConnectionAPI.Config;
using System.Net;
using System.Net.Mail;

namespace ConnectionAPI.Services
{
    public class AppSendMailResponse
    {
        public bool Success { get; set; } = false;
        /* public string HtmlBody { get; set; } */
    }
    public class MailData
    {
        public string ReplyToName { get; set; }
        public string ReplyToAddress { get; set; }
        public string From { get; set; }
        public string FromName { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string MailBody { get; set; }
        public string BaseUrl { get; set; }
    }

    public static class MailService
    {
        public static async Task<AppSendMailResponse> SendMail(MailData data)
        {

            SmtpClient client = new SmtpClient("in-v3.mailjet.com", 587)
            {
                Credentials = new NetworkCredential("f2d9167c9956510a7c950db622fea08e", "985782e21c867050c8ad56f05b04f4ba"),
                EnableSsl = true,
            };

            var appSendMailResponse = new AppSendMailResponse();

            if (data == null || data.To == null)
                return appSendMailResponse;


            /* string baseUrl = Server.SiteBaseUrl, baseServerPath = Server.SiteBaseDirectory;

            if (!string.IsNullOrWhiteSpace(data.BaseUrl))
                baseUrl = data.BaseUrl;
 */

            MailAddress to = new MailAddress(data.To);
            MailAddress from = new MailAddress("admin@express-trip.com");
            MailAddress copy = new MailAddress(data.CC);

            MailMessage message = new MailMessage(from, to);
            message.CC.Add(copy);
            message.Body = data.MailBody;
            message.Subject = data.Subject;
            message.IsBodyHtml = true;


            Console.WriteLine("Sending an email message to {0} by using the SMTP host {1}.",
                  to.Address, client.Host);
            try
            {
                client.Send(message);
                appSendMailResponse.Success = true;

            }
            catch (SmtpException Exception)
            {
                Console.WriteLine(Exception.ToString());
            }

            return appSendMailResponse;
        }
    }
}