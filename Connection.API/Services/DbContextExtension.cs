using ConnectionAPI.DAL;
using ConnectionAPI.DAL.DTO;
using System.Diagnostics;
using ConnectionAPI.DAL.Models;
using AutoMapper;
using Microsoft.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;
using ConnectionAPI.Config;
using Microsoft.EntityFrameworkCore;

namespace ConnectionAPI.Services
{
    public static class DbContextExtension
    {
        public static async Task CreateInitialData(this DataContext context, IMapper mapper, HttpClient httpClient)
        {
            try
            {
                  // await CreateInitialTypes(context, mapper);
                  // await CreateInitialAppValue(context, mapper, httpClient);
                  // await CreateRoles(context);
                  // await CreateInitialAgencies(context, mapper);
                  // await CreateInitialUser(context, mapper);
                   await CreateCities(context);
            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public static async Task CreateInitialAgencies(DataContext context, IMapper mapper)
        {
            try
            {
                var Agencies = new List<Agency>
                {
                    new Agency
                    {
                        Name = "Finexs voyage",
                        Ref = "finexs_voyage",
                        TransportType = TransportType.Bus,
                        Vehicles = new List<Auto>
                        {
                            new Auto
                            {
                                Matricule = "AB1234",
                                Ref = "finex_ab1234",
                                NbPlace = 40,
                            }
                        },
                    },
                    new Agency
                    {
                        Name = "Touristique Express",
                        Ref = "touristique_express",
                        TransportType = TransportType.Bus,
                        Vehicles = new List<Auto>
                        {
                            new Auto
                            {
                                Matricule = "AB1334",
                                Ref = "touristique_ab1234",
                                NbPlace = 40,
                            }
                        }
                    },
                };
                var AgenciesToCreate = new List<Agency>();
                foreach (var item in Agencies)
                {
                    var agencyResponse = await context.AgenciesReadOnly.Where(x => x.Ref == item.Ref).FirstOrDefaultAsync();
                    if (agencyResponse != null)
                        continue;

                    AgenciesToCreate.Add(item);
                }

                if (AgenciesToCreate != null && AgenciesToCreate.Count > 0)
                {
                    await context.Agencies.AddRangeAsync(AgenciesToCreate);
                    await context.SaveChangesAsync();
                }


            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public static async Task CreateInitialUser(DataContext context, IMapper mapper)
        {
            try
            {
                var superAdminRole = context.RolesReadOnly.Where(x => x.RoleCode == "SuperAdmin").FirstOrDefault();
                var agencyFinex = context.AgenciesReadOnly.Where(x => x.Ref == "finexs_voyage").FirstOrDefault();
                var touristiqueFinex = context.AgenciesReadOnly.Where(x => x.Ref == "touristique_express").FirstOrDefault();


                if (superAdminRole == null)
                    throw new Exception("Role undefined");

                List<UserDto> usersDto = new List<UserDto>();

                if (touristiqueFinex != null)
                {
                    usersDto.Add(new UserDto
                    {
                        Email = "contact@travel.com",
                        Password = "admin",
                        LastName = "chancel",
                        FirstName = "simo",
                        AgencyID = touristiqueFinex.ID,
                        IsActive = true,
                        UserRoles = new List<UserRoleDto>
                        {
                            new UserRoleDto
                            {
                                RoleID = superAdminRole.ID,
                            }
                        },
                    });
                }

                if (agencyFinex != null)
                {
                    usersDto.Add(
                        new UserDto
                        {
                            Email = "john@travel.com",
                            Password = "admin",
                            LastName = "john",
                            FirstName = "doe",
                            AgencyID = agencyFinex.ID,
                            IsActive = true,
                            UserRoles = new List<UserRoleDto>
                        {
                            new UserRoleDto
                            {
                                RoleID = superAdminRole.ID,
                            }
                        }
                        });
                }

                List<User> users = new List<User>();

                foreach (UserDto userDto in usersDto)
                {
                    User user = new User();

                    var userResponse = context.UsersReadOnly.Where(x => x.Email == userDto.Email).FirstOrDefault();
                    if (userResponse != null)
                        continue;

                    UsersService.CreatePasswordHash(userDto.Password, out byte[] passwordHash, out byte[] passwordSalt);

                    mapper.Map<UserDto, User>(userDto, user);
                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;

                    users.Add(user);
                }

                await context.Users.AddRangeAsync(users);
                await context.SaveChangesAsync();

            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }
        public static async Task CreateRoles(DataContext context)
        {
            try
            {

                Role[] defaultRoles = new Role[]
                {
                    new Role
                    {
                        RoleCode = "SuperAdmin",
                        RoleName = "super-admin"
                    },
                    new Role
                    {
                        RoleCode = "Admin",
                        RoleName = "admin"
                    },
                    new Role
                    {
                        RoleCode = "Employee",
                        RoleName = "employé"
                    },
                    new Role
                    {
                        RoleCode = "Client",
                        RoleName = "client"
                    }
                };


                foreach (var role in defaultRoles)
                {
                    var roleResponse = context.RolesReadOnly.Where(x => x.RoleCode == role.RoleCode).FirstOrDefault();

                    if (roleResponse != null)
                        continue;

                    await context.Roles.AddAsync(role);
                }
                await context.SaveChangesAsync();

            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public static async Task CreateInitialTypes(DataContext context, IMapper mapper)
        {
            try
            {
                var appTypeDtos = new List<AppTypeDto>
                {
                    new AppTypeDto
                    {
                        Code = AppTypeConstant.CityType,
                    }
                };

                var appTypes = new List<AppType>();

                foreach (var item in appTypeDtos)
                {
                    var appType = context.AppTypes.Where(x => x.Code == item.Code).FirstOrDefault();
                    if (appType != null)
                        continue;

                    var appTypeCreate = new AppType();
                    appTypeCreate = mapper.Map<AppTypeDto, AppType>(item);

                    if (appTypeCreate != null)
                        appTypes.Add(appTypeCreate);
                }

                if (appTypes != null && appTypes.Count > 0)
                {
                    context.AppTypes.AddRange(appTypes);
                    context.SaveChanges();
                }

            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public static async Task CreateInitialAppValue(DataContext context, IMapper mapper, HttpClient httpClient)
        {
            try
            {
                httpClient.BaseAddress = new Uri("https://public.opendatasoft.com");
                httpClient.DefaultRequestHeaders.Add(
                    HeaderNames.Accept, "application/json"
                );

                httpClient.DefaultRequestHeaders.Add(
                    HeaderNames.UserAgent, "HttpRequestsSample"
                );

                var citiesResponse = await httpClient.GetFromJsonAsync<CityApiType>("/api/records/1.0/search/?dataset=geonames-all-cities-with-a-population-1000&q=&rows=169&sort=name&facet=feature_code&facet=cou_name_en&facet=timezone&refine.cou_name_en=Cameroon");
                if (citiesResponse != null && citiesResponse.records != null && citiesResponse.records?.Count > 0)
                {

                    var cityType = new AppType
                    {
                        Code = AppTypeConstant.CityType,
                        Label = "Type villes",
                        AppValues = new List<AppValue>(),
                    };
                    

                    foreach (var item in citiesResponse.records)
                    {
                        var cityValueResponse = await context.AppValues.Where(x => x.Code == item.fields!.name).FirstOrDefaultAsync();
                        if (cityValueResponse != null)
                            continue;

                        cityType.AppValues.Add(
                            new AppValue
                            {
                                Code = item.fields!.name,
                                Label = item.fields!.name
                            }
                        );

                    }

                    if (cityType.AppValues != null && cityType.AppValues.Count > 0)
                    {
                        await context.AppTypes.AddAsync(cityType);
                        await context.SaveChangesAsync();
                    }
                }
            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public static async Task CreateCities(DataContext context)
        {
            try
            {
                var citiesTypeResponse = await context.AppTypesReadOnly.Where(x => x.Code == AppTypeConstant.CityType).
                                                                            Include(x => x.AppValues).
                                                                            FirstOrDefaultAsync();

                if (citiesTypeResponse == null || citiesTypeResponse.AppValues.Count < 1)
                    throw new Exception("no cities type found !");

                var cities = new List<City>();

                foreach (var item in citiesTypeResponse.AppValues)
                {
                    var cityResponse = await context.Cities.Where(x => x.CityValueID == item.ID).FirstOrDefaultAsync();

                    if (cityResponse != null)
                        continue;

                    var city = new City
                    {
                        CityValueID = item.ID,
                    };

                    cities.Add(city);
                }

                if (cities.Count > 0)
                {
                    context.Cities.AddRange(cities);
                    context.SaveChanges();
                }
            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }
    }

    public class CityApiType
    {
        public List<CityFieldType>? records { get; set; }
    }

    public class CityRecordType
    {
        public string? name { get; set; }
    }

    public class CityFieldType
    {
        public CityRecordType? fields { get; set; }
    }
}