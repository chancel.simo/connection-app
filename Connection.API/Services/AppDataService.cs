using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;


namespace ConnectionAPI.Services
{
    public class AppDataService
    {
        public static async Task<GetAppTypeResponse> GetAppType(DataContext context, IMapper mapper, string Code)
        {
            var response = new GetAppTypeResponse();
            try
            {
                var appTypeResponse = await context.AppTypesReadOnly.Where(x => x.Code == Code).Include(x => x.AppValues).FirstOrDefaultAsync();
                if (appTypeResponse != null)
                {
                    response.AppType = new AppTypeDto();
                    mapper.Map<AppType, AppTypeDto>(appTypeResponse, response.AppType);
                }

                response.Success = true;
            }
            catch (System.Exception exception)
            {
                response.HandleResponse(exception.Message);
            }

            return response;
        }

        public static async Task<CitiesResponse> GetCities(DataContext context, IMapper mapper)
        {
            var response = new CitiesResponse();
            try
            {
                var citiesResponse = await context.CitiesReadOnly.Include(x => x.CityValue).ToListAsync();
                if (citiesResponse != null)
                {
                    response.Cities = new List<CityDto>();
                    mapper.Map<List<City>, List<CityDto>>(citiesResponse, response.Cities);
                }

                response.Success = true;
            }
            catch (System.Exception exception)
            {
                response.HandleResponse(exception.Message);
            }
            return response;
        }
    }
}