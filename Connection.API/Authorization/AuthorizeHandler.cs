using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using ConnectionAPI.DAL;
using ConnectionAPI.Config;
using ConnectionAPI.Services;
using ConnectionAPI.DAL.DTO;
using AutoMapper;


namespace ConnectionAPI.Authorization
{
    public class AuthorizeHandler : AuthorizationHandler<RoleAuthorizeRequirement>
    {
        DataContext _context;
        IMapper _mapper;
        IConfiguration _configuration;

        public AuthorizeHandler(DataContext context, IMapper mapper, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleAuthorizeRequirement requirement)
        {
            if (_context != null)
                GlobalService.InitSession(_context, _mapper, _configuration);

            if (MySession.Instance.CurrentUser != null)
            {
                var roles = MySession.Instance.CurrentUser.UserRoles?.Select(x => x.Role?.RoleCode).ToList();
                if (roles != null)
                {
                    if (requirement.Roles.Contains(Constants.AdminRoleName) &&
                                    (
                                        roles.Contains(Constants.AdminRoleName) ||
                                        roles.Contains(Constants.SuperAdminRoleName)
                                    )
                        )
                    {
                        context.Succeed(requirement);
                    }
                    else if (requirement.Roles.Contains(Constants.EmployeeRoleName) &&
                                        (
                                         roles.Contains(Constants.EmployeeRoleName) ||
                                         roles.Contains(Constants.AdminRoleName) ||
                                         roles.Contains(Constants.SuperAdminRoleName)
                                        )
                            )
                    {
                        context.Succeed(requirement);
                    }
                    else if (requirement.Roles.Contains(Constants.SuperAdminRoleName) && roles.Contains(Constants.SuperAdminRoleName))
                    {
                        context.Succeed(requirement);
                    }
                }
            }

            return Task.CompletedTask;
        }
    }
}