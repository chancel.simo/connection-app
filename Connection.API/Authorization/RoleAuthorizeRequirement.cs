using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;


namespace ConnectionAPI.Authorization
{
    public class RoleAuthorizeRequirement : IAuthorizationRequirement
    {
        public IEnumerable<string> Roles;
        public RoleAuthorizeRequirement(IEnumerable<string> roles)
        {
            Roles = roles;
        }
    }
}