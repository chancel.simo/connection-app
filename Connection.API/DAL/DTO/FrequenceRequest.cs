using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class FrequenceRequest
    {
        public List<FrequenceDto> Frequences { get; set; }
    }
}