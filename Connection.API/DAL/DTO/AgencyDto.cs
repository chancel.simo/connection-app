using System.ComponentModel.DataAnnotations.Schema;
using ConnectionAPI.Config;

namespace ConnectionAPI.DAL.DTO;

public class AgencyDto
{
    public int ID { get; set; }
    public string Name { get; set; } = string.Empty;
    public TransportType TransportType { get; set; }
    public string? LogoPath { get; set; }
    public string? LogoName { get; set; }
    public string Ref { get; set; } = string.Empty;
    public List<UserDto>? Employees { get; set; }
    public List<AutoDto>? Vehicles { get; set; }
    public List<FrequenceDto>? Frequences { get; set; }

}