using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.DTO
{
    public class PriceTripDto
    {
        public int ID { get; set; }
        public int TripID { get; set; }
        public TripDto? Trip { get; set; }
        public ArretDto? DepartArret { get; set; }
        public int DepartArretID { get; set; }
        public ArretDto? ArrivalArret { get; set; }
        public int ArrivalArretID { get; set; }
        public int Price { get; set; }
    }
}