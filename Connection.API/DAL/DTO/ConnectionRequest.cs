using System.ComponentModel;

namespace ConnectionAPI.DAL.DTO
{

    public enum TimeSlotType
    {
        [Description("before_6")]
        Before6 = 1,

        [Description("between6_12")]
        Between6_12 = 2,

        [Description("Between12_18")]
        Between12_18 = 3,

        [Description("after_18")]
        After18 = 4,

    }

    public class ConnectionRequest
    {
        public int? EmployeeID { get; set; }
        public int? DepartArretID { get; set; }
        public int? ArrivalArretID { get; set; }
        public DateTime? StartDate { get; set; }
        public bool? EarliestDeparture { get; set; }
        public bool? LowestPrice { get; set; }
        public TimeSlotType? TimeSlot { get; set; }
    }
}