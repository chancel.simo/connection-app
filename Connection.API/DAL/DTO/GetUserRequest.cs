namespace ConnectionAPI.DAL.DTO
{
    public class GetUserRequest
    {
        public int? AgencyID { get; set; }
        public bool GetOnlyEmployee { get; set; }
        public bool GetOnlyAdmin { get; set; }
    }
}