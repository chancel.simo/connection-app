using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class FrequenceDto
    {
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public bool Lun { get; set; }
        public bool Mar { get; set; }
        public bool Mer { get; set; }
        public bool Jeu { get; set; }
        public bool Ven { get; set; }
        public bool Sam { get; set; }
        public bool Dim { get; set; }
        public int? AgencyID { get; set; }
        public AgencyDto? Agency { get; set; }
    }
}