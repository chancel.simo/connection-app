using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class AuthenticationTokenDto
    {
        public UserDto? User { get; set; }
        public int? UserID { get; set; }
        public string Token { get; set; } = string.Empty;
        public DateTime? CreationDate { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}