using ConnectionAPI.Config;
namespace ConnectionAPI.DAL.DTO;

public class UserDto
{
    public int ID { get; set; }
    public string LastName { get; set; } = string.Empty;
    public string FirstName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string? Password { get; set; }
    public string FullName { get; set; } = string.Empty;
    public string? AccessToken { get; set; }
    public int AgencyID { get; set; }
    public AgencyDto? Agency { get; set; }
    public List<UserRoleDto>? UserRoles { get; set; }
    public List<AuthenticationTokenDto>? AuthenticationTokens { get; set; }
    public bool IsArchive { get; set; }
    public bool IsActive { get; set; }
    public string LoginToken { get; set; } = string.Empty;



    /* public bool IsSuperAdmin
    {
        get
        {
            return UserHasRole && UserRoles.Any(x => x.Role.RoleCode == Constants.SuperAdminRoleName);
        }
    }

    public bool IsAdmin
    {
        get
        {
            return UserHasRole && UserRoles.Any(x => x.Role.RoleCode == Constants.AdminRoleName);
        }
    } */

    /*  public bool IsEmployee
     {
         get
         {
             return UserHasRole && UserRoles.Any(x => x.Role.RoleCode == Constants.EmployeeRoleName);
         }
     } */


    private bool UserHasRole
    {
        get
        {
            return UserRoles != null && UserRoles.Count > 0;
        }
    }
}