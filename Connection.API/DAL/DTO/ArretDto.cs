using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class ArretDto
    {
        public int? ID { get; set; }
        public int? TripID { get; set; }
        public TripDto? Trip { get; set; }

        public int? CityID { get; set; }
        public CityDto? City { get; set; }

        public DateTime? DepartureTime { get; set; }

        public DateTime? ArrivalTime { get; set; }

        public String? Description { get; set; }
        public int Order { get; set; }
    }
}