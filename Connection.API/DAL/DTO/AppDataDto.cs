using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class AppTypeDto
    {
        public int? ID { get; set; }
        public string? Code { get; set; }

        public List<AppValueDto>? AppValues { get; set; }
    }

    public class AppValueDto
    {
        public int? ID { get; set; }
        public string? Label { get; set; }
        public string? Code { get; set; }

        public int? AppTypeID { get; set; }

        public AppTypeDto? AppType { get; set; }
    }
}