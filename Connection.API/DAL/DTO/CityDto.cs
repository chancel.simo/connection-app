using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class CityDto
    {
        public int ID { get; set; }
        public int? CityValueID { get; set; }
        public virtual AppValueDto? CityValue { get; set; }
    }
}