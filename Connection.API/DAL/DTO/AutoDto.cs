
using System.ComponentModel.DataAnnotations.Schema;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.DTO;

public class AutoDto
{
    public int? ID { get; set; }
    public string Ref { get; set; } = string.Empty;
    public int NbPlace { get; set; }
    public string? Matricule { get; set; } = string.Empty;
    public int? AgencyID { get; set; }
    public AgencyDto? Agency { get; set; }
    public List<ConnectionDto>? Connection { get; set; }
    public List<TripDto>? Trips { get; set; }
}