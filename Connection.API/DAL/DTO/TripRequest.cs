using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class TripRequest
    {
        public int? ConnectionID { get; set; }
    }
}