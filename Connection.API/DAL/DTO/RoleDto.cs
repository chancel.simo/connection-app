

namespace ConnectionAPI.DAL.DTO
{
    public class RoleDto
    {
        public int ID { get; set; }
        public string? RoleName { get; set; }
        public string? RoleCode { get; set; }
        public List<UserRoleDto>? UserRoles { get; set; }
    }
}