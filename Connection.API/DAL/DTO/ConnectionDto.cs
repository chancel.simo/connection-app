using ConnectionAPI.Config;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.DAL.DTO
{
    public class ConnectionDto
    {
        public int ID { get; set; }
        public int AgentID { get; set; }
        public UserDto? Agent { get; set; }

        public List<TripDto>? Trips { get; set; }

        public int? AutoID { get; set; }
        public AutoDto? Auto { get; set; }
    }
}