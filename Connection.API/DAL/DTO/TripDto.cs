using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectionAPI.DAL.DTO
{
    public class TripDto
    {
        public int ID { get; set; }
        public string Ref { get; set; } = string.Empty;
        public int ConnectionID { get; set; }
        public ConnectionDto? Connection { get; set; }

        public DateTime? DepartureTime { get; set; }
        public DateTime? ArrivalTime { get; set; }

        public int? FrequenceID { get; set; }
        public FrequenceDto? Frequence { get; set; }
        public List<ArretDto>? ListArrets { get; set; }

        public int? AutoID { get; set; }
        public AutoDto? Auto { get; set; }
        public int Price { get; set; }

        public List<PriceTripDto>? PriceTrips { get; set; }
        private int? RequestDepartureArretID { get; set; }
        private int? RequestArrivalArret { get; set; }

        public string? FullTripLabel
        {
            get
            {
                if (DepartureArret == null || TerminusArret == null)
                    return null;

                return DepartureArret.City?.CityValue?.Label + " - " + TerminusArret.City?.CityValue?.Label + "(" + DepartureArret.DepartureTime + " - " + TerminusArret.ArrivalTime + ")";
            }
        }
        public void SetRequestDepartureArretID(int ReqDepatureID)
        {
            RequestDepartureArretID = ReqDepatureID;
        }

        public void SetRequestArrivalArret(int ReqArrivalID)
        {
            RequestArrivalArret = ReqArrivalID;
        }

        public ArretDto? DepartureArret
        {
            get
            {
                if (ListArrets == null || ListArrets.Count == 0)
                    return null;

                else if (RequestDepartureArretID != null)
                    return ListArrets.FirstOrDefault(x => x.ID == RequestDepartureArretID);

                else
                    return ListArrets.OrderBy(x => x.Order).FirstOrDefault();
            }
        }

        public ArretDto? TerminusArret
        {
            get
            {
                if (ListArrets == null || ListArrets.Count == 0)
                    return null;
                else if (RequestArrivalArret != null)
                    return ListArrets.FirstOrDefault(x => x.ID == RequestArrivalArret);
                else
                    return ListArrets.OrderByDescending(x => x.Order).FirstOrDefault();
            }
        }

        public AgencyDto? Agency
        {
            get
            {
                if (Connection == null || Connection.Agent == null || Connection.Agent.Agency == null)
                    return null;
                else
                    return Connection.Agent.Agency;
            }
        }

        public Double? Duration
        {
            get
            {
                if (DepartureArret != null && TerminusArret != null)
                {
                    TimeSpan? ts = TerminusArret.ArrivalTime - DepartureArret.DepartureTime;
                    return ts?.TotalMilliseconds;
                }
                else
                    return null;
            }
        }

        public int? StopOccurence
        {
            get
            {
                if (DepartureArret != null && TerminusArret != null && ListArrets?.Count > 0)
                {
                    if (ListArrets.Count == 2)
                        return 0;
                    else if (ListArrets.Count > 2)
                        return (TerminusArret.Order - DepartureArret.Order) - 1;
                    else
                        return 0;

                }
                else
                    return null;
            }
        }
    }
}