using Microsoft.EntityFrameworkCore;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.Config;

namespace ConnectionAPI.DAL;

public class DataContext : DbContext
{
    private IConfiguration _configuration;
    public DataContext(
        DbContextOptions<DataContext> options,
        IConfiguration configuration
    ) : base(options)
    {
        _configuration = configuration;
    }

    public DbSet<User> Users { get; set; }
    public IQueryable<User> UsersReadOnly { get { return Users.AsNoTracking(); } }

    public DbSet<Connection> Connections { get; set; }
    public IQueryable<Connection> ConnectionsReadOnly { get { return Connections.AsNoTracking(); } }

    public DbSet<Role> Roles { get; set; }
    public IQueryable<Role> RolesReadOnly { get { return Roles.AsNoTracking(); } }

    public DbSet<City> Cities { get; set; }
    public IQueryable<City> CitiesReadOnly { get { return Cities.AsNoTracking(); } }

    public DbSet<Trip> Trips { get; set; }
    public IQueryable<Trip> TripsReadOnly { get { return Trips.AsNoTracking(); } }

    public DbSet<Frequence> Frequences { get; set; }
    public IQueryable<Frequence> FrequencesReadOnly { get { return Frequences.AsNoTracking(); } }

    public DbSet<AppType> AppTypes { get; set; }
    public DbSet<AppValue> AppValues { get; set; }

    public IQueryable<AppType> AppTypesReadOnly { get { return AppTypes.AsNoTracking(); } }
    public IQueryable<AppValue> AppValuesReadOnly { get { return AppValues.AsNoTracking(); } }

    public DbSet<Agency> Agencies { get; set; }
    public IQueryable<Agency> AgenciesReadOnly { get { return Agencies.AsNoTracking(); } }

    public DbSet<Arret> Arrets { get; set; }
    public IQueryable<Arret> ArretsReadOnly { get { return Arrets.AsNoTracking(); } }

    public DbSet<PriceTrip> PriceTrips { get; set; }
    public IQueryable<PriceTrip> PriceTripsReadOnly { get { return PriceTrips.AsNoTracking(); } }

    public static ILoggerFactory? MyLoggerFactory;
    protected void ConfigureForMysql(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = _configuration.GetConnectionString("MySqlConnectionString");
        optionsBuilder.UseMySql(connectionString, MySqlServerVersion.AutoDetect(connectionString),
        x =>
        {
            x.CommandTimeout(60);
        });
        optionsBuilder.EnableSensitiveDataLogging(true);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        ConfigureForMysql(optionsBuilder);
    }
}