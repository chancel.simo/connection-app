
using System.ComponentModel.DataAnnotations.Schema;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models;

[Table("auto")]
public class Auto : GenericModel
{
    public string Ref { get; set; } = string.Empty;
    public int NbPlace { get; set; }
    public string? Matricule { get; set; } = string.Empty;

    public int AgencyID { get; set; }

    [ForeignKey("AgencyID")]
    public virtual Agency? Agency { get; set; }

    public virtual ICollection<Connection>? Connection { get; set; }
    public virtual ICollection<Trip>? Trip { get; set; }
}