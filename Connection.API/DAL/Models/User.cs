
using System.ComponentModel.DataAnnotations.Schema;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models;

[Table("users")]
public class User : GenericModel
{
    public string LastName { get; set; } = string.Empty;
    public string FirstName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public virtual ICollection<UserRole> UserRoles { get; set; }
    public byte[]? PasswordHash { get; set; }
    public byte[]? PasswordSalt { get; set; }

    public int AgencyID { get; set; }

    [ForeignKey("AgencyID")]
    public virtual Agency Agency { get; set; }
    public List<AuthenticationToken>? AuthenticationTokens { get; set; }
    public bool IsArchive { get; set; }
    public bool IsActive { get; set; }
    public string LoginToken { get; set; } = string.Empty;
}
