using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class FrequenceResponse : GenericResponse
    {
        public FrequenceDto? Frequence { get; set; }
    }

    public class FrequencesResponse : GenericResponse
    {
        public List<FrequenceDto>? Frequences { get; set; }
    }
}