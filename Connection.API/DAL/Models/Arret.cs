using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectionAPI.DAL.Models
{
    [Table("arrets")]
    public class Arret : GenericModel
    {
        [Required]
        public int TripID { get; set; }

        [ForeignKey("TripID")]
        public virtual Trip Trip { get; set; }

        [Required]
        public int CityID { get; set; }

        [ForeignKey("CityID")]
        public virtual City? City { get; set; }

        public DateTime? DepartureTime { get; set; }

        public DateTime? ArrivalTime { get; set; }

        public String? Description { get; set; }
        public int Order { get; set; }
    }
}