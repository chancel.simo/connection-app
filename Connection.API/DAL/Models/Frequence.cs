using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectionAPI.DAL.Models
{
    [Table("frequence")]
    public class Frequence : GenericModel
    {
        public string Name { get; set; } = string.Empty;
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? Lun { get; set; }
        public bool? Mar { get; set; }
        public bool? Mer { get; set; }
        public bool? Jeu { get; set; }
        public bool? Ven { get; set; }
        public bool? Sam { get; set; }
        public bool? Dim { get; set; }
        public int? AgencyID { get; set; }

        [ForeignKey("AgencyID")]
        public Agency? Agency { get; set; }
    }
}