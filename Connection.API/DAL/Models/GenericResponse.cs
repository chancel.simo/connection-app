

namespace ConnectionAPI.DAL.Models
{
    public class GenericResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; } = string.Empty;
        public string? AccessToken { get; set; }

        public void HandleResponse(string message)
        {
            Message = message;
        }
    }
}