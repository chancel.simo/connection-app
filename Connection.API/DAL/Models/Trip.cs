using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace ConnectionAPI.DAL.Models
{
    [Table("trip")]
    public class Trip : GenericModel
    {
        public string Ref { get; set; } = string.Empty;
        public int ConnectionID { get; set; }

        [ForeignKey("ConnectionID")]
        public virtual Connection? Connection { get; set; }

        public DateTime? DepartureTime { get; set; }

        public DateTime? ArrivalTime { get; set; }

        public int FrequenceID { get; set; }

        [ForeignKey("FrequenceID")]
        public virtual Frequence? Frequence { get; set; }

        public virtual ICollection<Arret>? ListArrets { get; set; }

        public int AutoID { get; set; }

        [ForeignKey("AutoID")]
        public virtual Auto? Auto { get; set; }
        public virtual List<PriceTrip> PriceTrips { get; set; }
        public int Price { get; set; }
    }
}