using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectionAPI.DAL.Models
{
    [Table("roles")]
    public class Role : GenericModel
    {
        public string? RoleName { get; set; }
        public string? RoleCode { get; set; }

        public virtual ICollection<UserRole>? UserRoles { get; set; }
    }
}