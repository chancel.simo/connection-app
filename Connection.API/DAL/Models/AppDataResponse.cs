using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class GetAppTypesResponse : GenericResponse
    {
        public List<AppTypeDto>? AppTypes { get; set; }
    }

    public class GetAppTypeResponse : GenericResponse
    {
        public AppTypeDto? AppType { get; set; }
    }

    public class GetAppValuesResponse
    {
        public List<AppValueDto>? AppValues { get; set; }
    }
}