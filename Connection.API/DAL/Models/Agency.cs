using System.ComponentModel.DataAnnotations.Schema;
using ConnectionAPI.Config;

namespace ConnectionAPI.DAL.Models;

[Table("agencies")]
public class Agency : GenericModel
{
    public string Name { get; set; } = string.Empty;
    public string Ref { get; set; } = string.Empty;
    public TransportType TransportType { get; set; }
    public string? LogoPath { get; set; }
    public string? LogoName { get; set; }
    public virtual ICollection<User>? Employees { get; set; }
    public virtual ICollection<Auto>? Vehicles { get; set; }
    public virtual ICollection<Frequence>? Frequences { get; set; }
}