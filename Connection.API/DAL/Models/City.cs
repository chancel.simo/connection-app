using System.ComponentModel.DataAnnotations.Schema;


namespace ConnectionAPI.DAL.Models
{
    [Table("city")]
    public class City : GenericModel
    {
        // public string Label { get; set; } = string.Empty;

        public int? CityValueID { get; set; }

        [ForeignKey("CityValueID")]
        public virtual AppValue CityValue { get; set; }
    }
}