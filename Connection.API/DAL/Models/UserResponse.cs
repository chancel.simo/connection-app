using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class UserResponse : GenericResponse
    {
        public UserDto? User { get; set; }
        public string? access_token { get; set; }
    }

    public class UsersResponse : GenericResponse
    {
        public List<UserDto>? Users { get; set; }
    }
}