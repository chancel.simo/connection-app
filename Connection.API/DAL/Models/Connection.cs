using System.ComponentModel.DataAnnotations.Schema;
using ConnectionAPI.Config;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    [Table("connection")]
    public class Connection : GenericModel
    {
        public int AgentID { get; set; }

        [ForeignKey("AgentID")]
        public virtual User? Agent { get; set; }

        public virtual ICollection<Trip>? Trips { get; set; }

        public int AutoID { get; set; }

        [ForeignKey("AutoID")]
        public virtual Auto? Auto { get; set; }
    }
}