using System.ComponentModel.DataAnnotations.Schema;


namespace ConnectionAPI.DAL.Models
{
    [Table("price-trip")]
    public class PriceTrip : GenericModel
    {

        public int TripID { get; set; }

        [ForeignKey("TripID")]
        public virtual Trip Trip { get; set; }

        [ForeignKey("DepartArretID")]
        public virtual Arret DepartArret { get; set; }
        public int DepartArretID { get; set; }

        [ForeignKey("ArrivalArretID")]
        public virtual Arret ArrivalArret { get; set; }
        public int ArrivalArretID { get; set; }

        public int Price { get; set; }
    }
}