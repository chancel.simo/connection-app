using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectionAPI.DAL.Models
{
    [Table("apptype")]
    public class AppType : GenericModel
    {
        public string Code { get; set; } = string.Empty;
        public string Label { get; set; } = string.Empty;
        public virtual ICollection<AppValue>? AppValues { get; set; }
    }

    [Table("appvalue")]
    public class AppValue : GenericModel
    {
        public string? Label { get; set; }
        public string? Code { get; set; }

        public int? AppTypeID { get; set; }

        [ForeignKey("AppTypeID")]
        public virtual AppType? AppType { get; set; }
    }
}