using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class TripsResponse : GenericResponse
    {
        public List<TripDto> TripsList { get; set; }
    }

    public class TripResponse : GenericResponse
    {
        public TripDto Trip { get; set; }
    }
}