using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class ConnectionResponse : GenericResponse
    {
        public ConnectionDto? Connection { get; set; }
    }

    public class ConnectionsResponse : GenericResponse
    {
        public List<ConnectionDto>? Connections { get; set; }
    }
}