using System.Collections.Generic;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class AgencyResponse : GenericResponse
    {
        public AgencyDto Agency { get; set; }
    }

    public class AgenciesResponse : GenericResponse
    {
        public List<AgencyDto> Agencies { get; set; }
    }
}