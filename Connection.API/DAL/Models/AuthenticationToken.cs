using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace ConnectionAPI.DAL.Models
{
    public class AuthenticationToken : GenericModel
    {
        [ForeignKey("UserID")]
        public virtual User? User { get; set; }

        public int? UserID { get; set; }
        public string Token { get; set; } = string.Empty;
        public DateTime? CreationDate { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}