using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConnectionAPI.DAL.DTO;

namespace ConnectionAPI.DAL.Models
{
    public class CityResponse : GenericResponse
    {
        public CityDto City { get; set; }
    }

    public class CitiesResponse : GenericResponse
    {
        public List<CityDto> Cities { get; set; }
    }
}