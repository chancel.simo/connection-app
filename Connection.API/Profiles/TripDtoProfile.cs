using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles
{
    public class TripDtoProfile : Profile
    {
        public TripDtoProfile()
        {
            CreateMap<Trip, TripDto>();
            CreateMap<PriceTrip, PriceTripDto>();
            CreateMap<Arret, ArretDto>();
            CreateMap<Frequence, FrequenceDto>();
            CreateMap<Connection, ConnectionDto>();

            CreateMap<TripDto, Trip>();
            CreateMap<PriceTripDto, PriceTrip>();
            CreateMap<ArretDto, Arret>();
            CreateMap<FrequenceDto, Frequence>();
            CreateMap<ConnectionDto, Connection>();
        }
    }

    public class PriceTripProfile : Profile
    {
        public PriceTripProfile()
        {
            CreateMap<PriceTrip, PriceTripDto>();

            CreateMap<PriceTripDto, PriceTrip>();
        }
    }
}