using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;
using Microsoft.Extensions.Configuration;

namespace ConnectionAPI.Profiles
{
    public class UserDtoProfile : Profile
    {
        public UserDtoProfile()
        {
            CreateMap<User, UserDto>().
                ForMember(opt => opt.Password, opt => opt.Ignore());
            CreateMap<AuthenticationToken, AuthenticationTokenDto>();
            CreateMap<UserRole, UserRoleDto>();
            CreateMap<Role, RoleDto>();
            CreateMap<Connection, ConnectionDto>();
            CreateMap<Agency, AgencyDto>();
            CreateMap<Auto, AutoDto>();
            CreateMap<Frequence, FrequenceDto>();

            CreateMap<UserDto, User>().
                ForMember(opt => opt.PasswordHash, opt => opt.Ignore()).
                ForMember(opt => opt.PasswordSalt, opt => opt.Ignore());
            CreateMap<UserRoleDto, UserRole>();
            CreateMap<AuthenticationTokenDto, AuthenticationToken>();
            CreateMap<RoleDto, Role>();
            CreateMap<ConnectionDto, Connection>();
            CreateMap<AgencyDto, Agency>();
            CreateMap<AutoDto, Auto>();
            CreateMap<FrequenceDto, Frequence>();

        }
    }

    /*  public class UserDtoResolver : IValueResolver<UserDto, User>
     {
         public UserDtoResolver(IConfiguration configuration)
         {

         }

         public 
     } */
}