using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles
{
    public class AppTypeDtoProfile : Profile
    {
        public AppTypeDtoProfile()
        {
            CreateMap<AppType, AppTypeDto>();
            CreateMap<AppValue, AppValueDto>();

            CreateMap<AppTypeDto, AppType>();
            CreateMap<AppValueDto, AppValue>();
        }
    }

    public class AppValueDtoProfile : Profile
    {
        public AppValueDtoProfile()
        {
            CreateMap<AppValue, AppValueDto>();

            CreateMap<AppValueDto, AppValue>();
            CreateMap<AppTypeDto, AppType>();
        }
    }
}