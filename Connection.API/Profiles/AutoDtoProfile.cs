using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles
{
    public class AutoDtoProfile : Profile
    {
        public AutoDtoProfile()
        {
            CreateMap<Auto, AutoDto>();

            CreateMap<AutoDto, Auto>();
        }
    }
}