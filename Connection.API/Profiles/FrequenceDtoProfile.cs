using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles;

public class FrequenceDtoProfile : Profile
{
    public FrequenceDtoProfile()
    {
        CreateMap<Frequence, FrequenceDto>();

        CreateMap<FrequenceDto, Frequence>();
    }
}