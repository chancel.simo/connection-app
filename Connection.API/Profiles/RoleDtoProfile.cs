using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles
{
    public class RoleDtoProfile : Profile
    {
        public RoleDtoProfile()
        {
            CreateMap<Role, RoleDto>();
            CreateMap<UserRole, UserRoleDto>();

            CreateMap<RoleDto, Role>();
            CreateMap<UserRoleDto, UserRole>();
        }
    }
}