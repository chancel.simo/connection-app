using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles;

public class ArretDtoProfile : Profile
{
    public ArretDtoProfile()
    {
        CreateMap<Arret, ArretDto>();

        CreateMap<ArretDto, Arret>();
    }
}