using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles
{
    public class ConnectionDtoProfile : Profile
    {
        public ConnectionDtoProfile()
        {
            CreateMap<Connection, ConnectionDto>();
            CreateMap<Trip, TripDto>();
            CreateMap<PriceTrip, PriceTripDto>();
            CreateMap<Frequence, FrequenceDto>();
            CreateMap<Arret, ArretDto>();
            CreateMap<City, CityDto>();
            CreateMap<User, UserDto>();

            CreateMap<ConnectionDto, Connection>();
            CreateMap<TripDto, Trip>();
            CreateMap<PriceTripDto, PriceTrip>();
            CreateMap<FrequenceDto, Frequence>();
            CreateMap<ArretDto, Arret>();
            CreateMap<CityDto, City>();
            CreateMap<UserDto, User>();
        }
    }
}