using AutoMapper;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.DAL.Models;

namespace ConnectionAPI.Profiles;

public class AgencyDtoProfile : Profile
{
    public AgencyDtoProfile()
    {
        CreateMap<Agency, AgencyDto>();
        CreateMap<Auto, AutoDto>();

        CreateMap<AgencyDto, Agency>();
        CreateMap<AutoDto, Auto>();
    }
}