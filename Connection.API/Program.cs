using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.DependencyInjection;
using ConnectionAPI.DAL;
using System.Text;
using Microsoft.OpenApi.Models;
using ConnectionAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using ConnectionAPI;
using Swashbuckle.AspNetCore.Filters;
using ConnectionAPI.Config;
using ConnectionAPI.Utils.Swagger;
using AutoMapper;
using ConnectionAPI.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Net.Http;
using ConnectionAPI.utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.HttpOverrides;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

ConfigureServices(builder.Services, builder.Configuration, builder.Environment.IsDevelopment());

var app = builder.Build();

Configure(app, builder.Environment);

app.Run();

void ConfigureServices(IServiceCollection services, ConfigurationManager configuration, bool IsDevelopment)
{

    var connectionString = configuration.GetConnectionString("MySqlConnectionString");
    services.AddDbContext<DataContext>(options =>
        options.UseMySql(MySqlServerVersion.AutoDetect(connectionString)),
        ServiceLifetime.Transient
    );
    services.AddMvc();

    services.AddControllers().AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
        options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
    });
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

    var origins = new List<string>();

    if (IsDevelopment)
    {
        origins.AddRange(new List<string>
        {
            "http://localhost:5432",
            "http://localhost:5433"
        });
    }
    else
    {
        origins.AddRange(new List<string>
        {
            "https://www.express-trip.com",
            "https://express-trip.com",
            "https://myaccount.express-trip.com"
        });
    }

    services.AddCors(options =>
    {
        options.AddDefaultPolicy(
            builder =>
                        {
                            builder.AllowAnyHeader();
                            builder.AllowAnyMethod();
                            builder.AllowCredentials();
                            builder.AllowAnyOrigin();
                            builder.WithOrigins(origins.ToArray());
                        }
        );
    });



    services.AddAuthorization(options =>
        {
            options.AddPolicy(Constants.AdminRoleName, policy =>
            policy.Requirements.Add(new RoleAuthorizeRequirement(new string[] { Constants.AdminRoleName })));

            options.AddPolicy(Constants.EmployeeRoleName, policy =>
            policy.Requirements.Add(new RoleAuthorizeRequirement(new string[] { Constants.EmployeeRoleName })));

            options.AddPolicy(Constants.SuperAdminRoleName, policy =>
            policy.Requirements.Add(new RoleAuthorizeRequirement(new string[] { Constants.SuperAdminRoleName })));
        }
    );

    services.AddScoped<IAuthorizationHandler, AuthorizeHandler>();

    services.AddSwaggerGen(options =>
    {
        options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
            Description = "Standard Authorization Bearer",
            In = ParameterLocation.Header,
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey
        });

        options.OperationFilter<SecurityRequirementsOperationFilter>();
    });
    services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
       .AddJwtBearer(options =>
       {

           var tokenSecretKey = configuration.GetSection("TokenSecretKey").Value;
           var crmClientBaseUrl = configuration.GetSection("CRMClientBaseUrl").Value;
           var publicClientBaseUrl = configuration.GetSection("PublicClientBaseUrl").Value;

           options.TokenValidationParameters = new TokenValidationParameters
           {
               ValidateIssuer = true,
               ValidateAudience = true,
               ValidateLifetime = true,
               ValidateIssuerSigningKey = true,

               ValidIssuers = new List<string> { crmClientBaseUrl, publicClientBaseUrl },
               ValidAudiences = new List<string> { crmClientBaseUrl, publicClientBaseUrl },
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                  .GetBytes(tokenSecretKey)
               ),

           };
           /* options.TokenValidationParameters = new TokenValidationParameters
           {
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                   .GetBytes(tokenSecretKey)
               ),
               ValidateIssuer = false,
               ValidateAudience = false,
           }; */
       });
    services.AddDistributedMemoryCache();
    services.AddHttpClient<DatabaseService>();
    services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, Microsoft.AspNetCore.Http.HttpContextAccessor>();
    services.AddSingleton<IHttpContext, MyHttpContext>();
    services.AddSingleton<TravelSessionManager, MyTravelSessionManager>();
    services.AddSingleton<IConfiguration>(configuration);

    services.AddSession(options =>
    {
        options.IdleTimeout = TimeSpan.FromMinutes(30);
        options.Cookie.HttpOnly = true;
        options.Cookie.Name = "Connection-AspNetCoreSession";
    });

    services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    }).AddCookie(options =>
        {
            options.Events.OnRedirectToLogin = context =>
            {
                context.Response.StatusCode = 403;
                return Task.CompletedTask;
            };
        });

    services.AddMemoryCache();

    if (IsDevelopment)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v" + 1, new OpenApiInfo { Title = AppSettings.APITitle, Version = "v" + 1 });
            c.OperationFilter<SwaggerOperationNameFilter>();
            c.SchemaFilter<ModifySchemaFilter>();
            c.EnableAnnotations();
        });

        services.AddSwaggerGenNewtonsoftSupport();
    }
}

void Configure(WebApplication app, IWebHostEnvironment env)
{
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint(AppSettings.SwaggerEndPoint, AppSettings.APITitle);
        });

    }

    if (AppSettings.MustSeedDataBase)
    {
        using (var scope = app.Services.CreateScope())
        {
            var dbContext = scope.ServiceProvider.GetRequiredService<DataContext>();
            var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();
            var httpClient = scope.ServiceProvider.GetRequiredService<HttpClient>();

            DatabaseService.UpdateDatabaseIfNeeded(dbContext, mapper, httpClient);
        }
    }

    app.UseCors();

    app.UseSession();

    app.UseDefaultFiles();
    app.UseStaticFiles();

    if (app.Environment.IsProduction())
    {
        app.UseExceptionHandler("/Error");
        // app.UseHsts();
        //app.UseHttpsRedirection();
    }
    app.UseAuthentication();


    app.UseRouting();

    app.UseFileServer(new FileServerOptions()
    {
        FileProvider = new PhysicalFileProvider(
            Path.Combine(builder.Environment.ContentRootPath, "Upload")),
        RequestPath = "/files",
        EnableDefaultFiles = true,
        EnableDirectoryBrowsing = true
    });

    app.UseStaticFiles(new StaticFileOptions()
    {
        FileProvider = new PhysicalFileProvider(
            Path.Combine(builder.Environment.ContentRootPath, "Upload")),
        RequestPath = "/files"
    });


    app.UseAuthorization();
    app.MapControllers();


    app.UseEndpoints(endpoint =>
    {
        endpoint.MapControllers();

        if (app.Environment.IsDevelopment())
        {
            endpoint.MapGet("/", context =>
            {
                context.Response.Redirect("/swagger");
                return Task.FromResult(0);
            });
        }
        else
            endpoint.MapGet("/", (context) => context.Response.WriteAsync("API OK"));

    });

    /* app.UseForwardedHeaders(new ForwardedHeadersOptions
    {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
    }); */



    using (var scope = app.Services.CreateScope())
    {
        var travelSessionManager = scope.ServiceProvider.GetRequiredService<TravelSessionManager>();
        var httpContext = scope.ServiceProvider.GetRequiredService<IHttpContext>();
        var webEnvironment = scope.ServiceProvider.GetRequiredService<IWebHostEnvironment>();


        GlobalService.InitApp(travelSessionManager, httpContext, webEnvironment);
    }
}
