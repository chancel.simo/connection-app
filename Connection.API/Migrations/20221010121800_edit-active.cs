﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ConnectionAPI.Migrations
{
    public partial class editactive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isArchive",
                table: "users",
                newName: "IsArchive");

            migrationBuilder.RenameColumn(
                name: "isActive",
                table: "users",
                newName: "IsActive");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsArchive",
                table: "users",
                newName: "isArchive");

            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "users",
                newName: "isActive");
        }
    }
}
