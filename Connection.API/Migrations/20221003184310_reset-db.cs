﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ConnectionAPI.Migrations
{
    public partial class resetdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "agencies",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Ref = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TransportType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agencies", x => x.ID);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "apptype",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Label = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_apptype", x => x.ID);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    RoleCode = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.ID);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "auto",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Ref = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    NbPlace = table.Column<int>(type: "int", nullable: false),
                    Matricule = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AgencyID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_auto", x => x.ID);
                    table.ForeignKey(
                        name: "FK_auto_agencies_AgencyID",
                        column: x => x.AgencyID,
                        principalTable: "agencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "frequence",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Lun = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Mar = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Mer = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Jeu = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Ven = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sam = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Dim = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    AgencyID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_frequence", x => x.ID);
                    table.ForeignKey(
                        name: "FK_frequence_agencies_AgencyID",
                        column: x => x.AgencyID,
                        principalTable: "agencies",
                        principalColumn: "ID");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    LastName = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FirstName = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PasswordHash = table.Column<byte[]>(type: "longblob", nullable: true),
                    PasswordSalt = table.Column<byte[]>(type: "longblob", nullable: true),
                    AgencyID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_users_agencies_AgencyID",
                        column: x => x.AgencyID,
                        principalTable: "agencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "appvalue",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Label = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Code = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AppTypeID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_appvalue", x => x.ID);
                    table.ForeignKey(
                        name: "FK_appvalue_apptype_AppTypeID",
                        column: x => x.AppTypeID,
                        principalTable: "apptype",
                        principalColumn: "ID");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "AuthenticationToken",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(type: "int", nullable: true),
                    Token = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastLogin = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    RefreshTokenExpiryTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthenticationToken", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AuthenticationToken_users_UserID",
                        column: x => x.UserID,
                        principalTable: "users",
                        principalColumn: "ID");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "connection",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AgentID = table.Column<int>(type: "int", nullable: false),
                    AutoID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_connection", x => x.ID);
                    table.ForeignKey(
                        name: "FK_connection_auto_AutoID",
                        column: x => x.AutoID,
                        principalTable: "auto",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_connection_users_AgentID",
                        column: x => x.AgentID,
                        principalTable: "users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user-role",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(type: "int", nullable: false),
                    RoleID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user-role", x => x.ID);
                    table.ForeignKey(
                        name: "FK_user-role_roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "roles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user-role_users_UserID",
                        column: x => x.UserID,
                        principalTable: "users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "city",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CityValueID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_city", x => x.ID);
                    table.ForeignKey(
                        name: "FK_city_appvalue_CityValueID",
                        column: x => x.CityValueID,
                        principalTable: "appvalue",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "trip",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Ref = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConnectionID = table.Column<int>(type: "int", nullable: false),
                    DepartureTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    ArrivalTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    FrequenceID = table.Column<int>(type: "int", nullable: false),
                    AutoID = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trip", x => x.ID);
                    table.ForeignKey(
                        name: "FK_trip_auto_AutoID",
                        column: x => x.AutoID,
                        principalTable: "auto",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_trip_connection_ConnectionID",
                        column: x => x.ConnectionID,
                        principalTable: "connection",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_trip_frequence_FrequenceID",
                        column: x => x.FrequenceID,
                        principalTable: "frequence",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "arrets",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TripID = table.Column<int>(type: "int", nullable: false),
                    CityID = table.Column<int>(type: "int", nullable: false),
                    DepartureTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    ArrivalTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Description = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Order = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_arrets", x => x.ID);
                    table.ForeignKey(
                        name: "FK_arrets_city_CityID",
                        column: x => x.CityID,
                        principalTable: "city",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_arrets_trip_TripID",
                        column: x => x.TripID,
                        principalTable: "trip",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "price-trip",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TripID = table.Column<int>(type: "int", nullable: false),
                    DepartArretID = table.Column<int>(type: "int", nullable: false),
                    ArrivalArretID = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_price-trip", x => x.ID);
                    table.ForeignKey(
                        name: "FK_price-trip_arrets_ArrivalArretID",
                        column: x => x.ArrivalArretID,
                        principalTable: "arrets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_price-trip_arrets_DepartArretID",
                        column: x => x.DepartArretID,
                        principalTable: "arrets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_price-trip_trip_TripID",
                        column: x => x.TripID,
                        principalTable: "trip",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_appvalue_AppTypeID",
                table: "appvalue",
                column: "AppTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_arrets_CityID",
                table: "arrets",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_arrets_TripID",
                table: "arrets",
                column: "TripID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthenticationToken_UserID",
                table: "AuthenticationToken",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_auto_AgencyID",
                table: "auto",
                column: "AgencyID");

            migrationBuilder.CreateIndex(
                name: "IX_city_CityValueID",
                table: "city",
                column: "CityValueID");

            migrationBuilder.CreateIndex(
                name: "IX_connection_AgentID",
                table: "connection",
                column: "AgentID");

            migrationBuilder.CreateIndex(
                name: "IX_connection_AutoID",
                table: "connection",
                column: "AutoID");

            migrationBuilder.CreateIndex(
                name: "IX_frequence_AgencyID",
                table: "frequence",
                column: "AgencyID");

            migrationBuilder.CreateIndex(
                name: "IX_price-trip_ArrivalArretID",
                table: "price-trip",
                column: "ArrivalArretID");

            migrationBuilder.CreateIndex(
                name: "IX_price-trip_DepartArretID",
                table: "price-trip",
                column: "DepartArretID");

            migrationBuilder.CreateIndex(
                name: "IX_price-trip_TripID",
                table: "price-trip",
                column: "TripID");

            migrationBuilder.CreateIndex(
                name: "IX_trip_AutoID",
                table: "trip",
                column: "AutoID");

            migrationBuilder.CreateIndex(
                name: "IX_trip_ConnectionID",
                table: "trip",
                column: "ConnectionID");

            migrationBuilder.CreateIndex(
                name: "IX_trip_FrequenceID",
                table: "trip",
                column: "FrequenceID");

            migrationBuilder.CreateIndex(
                name: "IX_user-role_RoleID",
                table: "user-role",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_user-role_UserID",
                table: "user-role",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_users_AgencyID",
                table: "users",
                column: "AgencyID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthenticationToken");

            migrationBuilder.DropTable(
                name: "price-trip");

            migrationBuilder.DropTable(
                name: "user-role");

            migrationBuilder.DropTable(
                name: "arrets");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "city");

            migrationBuilder.DropTable(
                name: "trip");

            migrationBuilder.DropTable(
                name: "appvalue");

            migrationBuilder.DropTable(
                name: "connection");

            migrationBuilder.DropTable(
                name: "frequence");

            migrationBuilder.DropTable(
                name: "apptype");

            migrationBuilder.DropTable(
                name: "auto");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "agencies");
        }
    }
}
