﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ConnectionAPI.Migrations
{
    public partial class archiveuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isArchive",
                table: "users",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isArchive",
                table: "users");
        }
    }
}
