﻿// <auto-generated />
using System;
using ConnectionAPI.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace ConnectionAPI.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20221003184310_reset-db")]
    partial class resetdb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Agency", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<string>("Ref")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<int>("TransportType")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.ToTable("agencies");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.AppType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<string>("Label")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.HasKey("ID");

                    b.ToTable("apptype");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.AppValue", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int?>("AppTypeID")
                        .HasColumnType("int");

                    b.Property<string>("Code")
                        .HasColumnType("longtext");

                    b.Property<string>("Label")
                        .HasColumnType("longtext");

                    b.HasKey("ID");

                    b.HasIndex("AppTypeID");

                    b.ToTable("appvalue");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Arret", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime?>("ArrivalTime")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("CityID")
                        .HasColumnType("int");

                    b.Property<DateTime?>("DepartureTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Description")
                        .HasColumnType("longtext");

                    b.Property<int>("Order")
                        .HasColumnType("int");

                    b.Property<int>("TripID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.HasIndex("TripID");

                    b.ToTable("arrets");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.AuthenticationToken", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreationDate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("LastLogin")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("RefreshTokenExpiryTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Token")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<int?>("UserID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("UserID");

                    b.ToTable("AuthenticationToken");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Auto", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("AgencyID")
                        .HasColumnType("int");

                    b.Property<string>("Matricule")
                        .HasColumnType("longtext");

                    b.Property<int>("NbPlace")
                        .HasColumnType("int");

                    b.Property<string>("Ref")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.HasKey("ID");

                    b.HasIndex("AgencyID");

                    b.ToTable("auto");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.City", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int?>("CityValueID")
                        .IsRequired()
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("CityValueID");

                    b.ToTable("city");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Connection", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("AgentID")
                        .HasColumnType("int");

                    b.Property<int>("AutoID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("AgentID");

                    b.HasIndex("AutoID");

                    b.ToTable("connection");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Frequence", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int?>("AgencyID")
                        .HasColumnType("int");

                    b.Property<bool?>("Dim")
                        .HasColumnType("tinyint(1)");

                    b.Property<DateTime?>("EndDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool?>("Jeu")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool?>("Lun")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool?>("Mar")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool?>("Mer")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<bool?>("Sam")
                        .HasColumnType("tinyint(1)");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("datetime(6)");

                    b.Property<bool?>("Ven")
                        .HasColumnType("tinyint(1)");

                    b.HasKey("ID");

                    b.HasIndex("AgencyID");

                    b.ToTable("frequence");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.PriceTrip", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("ArrivalArretID")
                        .HasColumnType("int");

                    b.Property<int>("DepartArretID")
                        .HasColumnType("int");

                    b.Property<int>("Price")
                        .HasColumnType("int");

                    b.Property<int>("TripID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("ArrivalArretID");

                    b.HasIndex("DepartArretID");

                    b.HasIndex("TripID");

                    b.ToTable("price-trip");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Role", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("RoleCode")
                        .HasColumnType("longtext");

                    b.Property<string>("RoleName")
                        .HasColumnType("longtext");

                    b.HasKey("ID");

                    b.ToTable("roles");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Trip", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime?>("ArrivalTime")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("AutoID")
                        .HasColumnType("int");

                    b.Property<int>("ConnectionID")
                        .HasColumnType("int");

                    b.Property<DateTime?>("DepartureTime")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("FrequenceID")
                        .HasColumnType("int");

                    b.Property<int>("Price")
                        .HasColumnType("int");

                    b.Property<string>("Ref")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.HasKey("ID");

                    b.HasIndex("AutoID");

                    b.HasIndex("ConnectionID");

                    b.HasIndex("FrequenceID");

                    b.ToTable("trip");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("AgencyID")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<byte[]>("PasswordHash")
                        .HasColumnType("longblob");

                    b.Property<byte[]>("PasswordSalt")
                        .HasColumnType("longblob");

                    b.HasKey("ID");

                    b.HasIndex("AgencyID");

                    b.ToTable("users");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.UserRole", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("RoleID")
                        .HasColumnType("int");

                    b.Property<int?>("UserID")
                        .IsRequired()
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("RoleID");

                    b.HasIndex("UserID");

                    b.ToTable("user-role");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.AppValue", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.AppType", "AppType")
                        .WithMany("AppValues")
                        .HasForeignKey("AppTypeID");

                    b.Navigation("AppType");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Arret", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.City", "City")
                        .WithMany()
                        .HasForeignKey("CityID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.Trip", "Trip")
                        .WithMany("ListArrets")
                        .HasForeignKey("TripID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("City");

                    b.Navigation("Trip");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.AuthenticationToken", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.User", "User")
                        .WithMany("AuthenticationTokens")
                        .HasForeignKey("UserID");

                    b.Navigation("User");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Auto", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.Agency", "Agency")
                        .WithMany("Vehicles")
                        .HasForeignKey("AgencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Agency");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.City", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.AppValue", "CityValue")
                        .WithMany()
                        .HasForeignKey("CityValueID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("CityValue");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Connection", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.User", "Agent")
                        .WithMany()
                        .HasForeignKey("AgentID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.Auto", "Auto")
                        .WithMany("Connection")
                        .HasForeignKey("AutoID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Agent");

                    b.Navigation("Auto");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Frequence", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.Agency", "Agency")
                        .WithMany("Frequences")
                        .HasForeignKey("AgencyID");

                    b.Navigation("Agency");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.PriceTrip", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.Arret", "ArrivalArret")
                        .WithMany()
                        .HasForeignKey("ArrivalArretID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.Arret", "DepartArret")
                        .WithMany()
                        .HasForeignKey("DepartArretID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.Trip", "Trip")
                        .WithMany("PriceTrips")
                        .HasForeignKey("TripID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ArrivalArret");

                    b.Navigation("DepartArret");

                    b.Navigation("Trip");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Trip", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.Auto", "Auto")
                        .WithMany("Trip")
                        .HasForeignKey("AutoID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.Connection", "Connection")
                        .WithMany("Trips")
                        .HasForeignKey("ConnectionID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.Frequence", "Frequence")
                        .WithMany()
                        .HasForeignKey("FrequenceID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Auto");

                    b.Navigation("Connection");

                    b.Navigation("Frequence");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.User", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.Agency", "Agency")
                        .WithMany("Employees")
                        .HasForeignKey("AgencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Agency");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.UserRole", b =>
                {
                    b.HasOne("ConnectionAPI.DAL.Models.Role", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ConnectionAPI.DAL.Models.User", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("User");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Agency", b =>
                {
                    b.Navigation("Employees");

                    b.Navigation("Frequences");

                    b.Navigation("Vehicles");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.AppType", b =>
                {
                    b.Navigation("AppValues");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Auto", b =>
                {
                    b.Navigation("Connection");

                    b.Navigation("Trip");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Connection", b =>
                {
                    b.Navigation("Trips");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Role", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.Trip", b =>
                {
                    b.Navigation("ListArrets");

                    b.Navigation("PriceTrips");
                });

            modelBuilder.Entity("ConnectionAPI.DAL.Models.User", b =>
                {
                    b.Navigation("AuthenticationTokens");

                    b.Navigation("UserRoles");
                });
#pragma warning restore 612, 618
        }
    }
}
