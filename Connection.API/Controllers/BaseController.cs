using Microsoft.AspNetCore.Mvc;
using ConnectionAPI.DAL;
using Microsoft.AspNetCore.SignalR;
using AutoMapper;
using ConnectionAPI.Services;


namespace ConnectionAPI.Controllers
{
    public class BaseController : Controller
    {
        public readonly DataContext CurrentContext;
        public readonly IMapper Mapper;
        public readonly IConfiguration Configuration;
        public BaseController(DataContext context, IMapper mapper, IConfiguration configuration)
        {
            CurrentContext = context;
            Mapper = mapper;
            Configuration = configuration;
            GlobalService.InitSession(CurrentContext, Mapper, configuration);
        }

    }
}