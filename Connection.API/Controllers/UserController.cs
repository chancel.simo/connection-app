using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL.DTO;
using Microsoft.EntityFrameworkCore;
using ConnectionAPI.Services;
using System.Security.Claims;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using AutoMapper;
using ConnectionAPI.Config;

namespace ConnectionAPI.Controllers;

[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class UserController : BaseController
{
    protected IConfiguration _configuration;
    public UserController(DataContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper, configuration)
    {
        _configuration = configuration;
    }

    [HttpGet("GetUser")]
    [SwaggerOperation("GetUser")]
    [Authorize(Constants.AdminRoleName)]
    public async Task<ActionResult<UserResponse>> GetUser(int ID)
    {
        return Ok(await UsersService.GetUser(CurrentContext, Mapper, ID));
    }

    [HttpPost("GetUsers")]
    [SwaggerOperation("GetUsers")]
    [Authorize(Constants.AdminRoleName)]
    public async Task<ActionResult<UserResponse>> GetUsers([FromBody] GetUserRequest request)
    {
        return Ok(await UsersService.GetUsers(CurrentContext, Mapper, request));
    }

    [HttpPost("register")]
    [SwaggerOperation("Register")]
    public async Task<ActionResult<UserResponse>> Register([FromBody] UserDto request)
    {
        return Ok(await UsersService.RegisterAsync(CurrentContext, Mapper, request));
    }

    [HttpPost("login")]
    [SwaggerOperation("Login")]
    [SwaggerResponse(200, "LoginResponse", typeof(UserResponse))]
    public async Task<ActionResult<UserResponse>> Login([FromBody] LoginRequest request)
    {
        if (request == null)
            return BadRequest("invalid credentials");

        return Ok(await UsersService.Login(CurrentContext, Mapper, _configuration, HttpContext, request));
    }

    [HttpPost("saveUser")]
    [SwaggerOperation("saveUser")]
    /* [Authorize(Constants.EmployeeRoleName)] */
    [SwaggerResponse(200, "UserResponse", typeof(UserResponse))]
    public async Task<ActionResult<UserResponse>> SaveUser([FromBody] UserDto user)
    {
        return Ok(await UsersService.SaveUser(CurrentContext, Mapper, user));
    }

    [HttpPost("saveFrequences")]
    [SwaggerOperation("saveFrequences")]
    [SwaggerResponse(200, "FrequencesResponse", typeof(FrequencesResponse))]
    public async Task<ActionResult<FrequencesResponse>> SaveFrequences([FromBody] FrequenceRequest request)
    {
        return Ok(await UsersService.SaveFrequences(CurrentContext, Mapper, request));
    }

    [HttpPost("refresh-token")]
    [SwaggerOperation("refreshToken")]
    [SwaggerResponse(200, "GenericResponse", typeof(GenericResponse))]
    public ActionResult<GenericResponse> RefreshToken()
    {
        var response = new GenericResponse();
        try
        {
            var refreshToken = Request.Cookies[AppSettings.TravelCookieKey];
            if (refreshToken == null || string.IsNullOrWhiteSpace(refreshToken))
                throw new Exception("Refresh token null - logout");

            var connecteduser = UsersService.GetUserByRefreshToken(CurrentContext, refreshToken, Mapper);
            if (connecteduser.AuthenticationTokens == null)
            {
                throw new Exception("user not exist or has been deleted");
            }

            if (connecteduser.AuthenticationTokens.Any(x => !x.Token.Equals(refreshToken)))
            {
                throw new Exception("invalid refresh token");
            }
            else if (connecteduser.AuthenticationTokens.Any(x => x.RefreshTokenExpiryTime < DateTime.Now))
            {
                throw new Exception("token expired");
            }

            string token = UsersService.CreateToken(connecteduser, _configuration);
            var newRefreshToken = UsersService.GenerateRefreshToken();
            UsersService.SetRefreshToken(newRefreshToken, connecteduser);

            response.AccessToken = token;
            response.Success = true;
        }
        catch (Exception Exception)
        {
            response.HandleResponse(Exception.Message);
        }
        return Ok(response);
    }

    [HttpGet("Logout")]
    [SwaggerOperation("Logout")]
    [SwaggerResponse(200, "", typeof(GenericResponse))]
    public async Task<ActionResult> Logout()
    {
        return Ok(await UsersService.Logout(CurrentContext));
    }

    [HttpGet("GetUserByToken")]
    [SwaggerOperation("GetUserByToken")]
    [SwaggerResponse(200, "", typeof(UserResponse))]
    public async Task<ActionResult<UserResponse>> GetUserByToken(string token)
    {
        return Ok(await UsersService.GetUserByToken(CurrentContext, Mapper, token));
    }

    [HttpPost("RegisterWithToken")]
    [SwaggerOperation("RegisterWithToken")]
    [SwaggerResponse(200, "", typeof(GenericResponse))]
    public async Task<ActionResult<GenericResponse>> RegisterWithToken([FromBody] UserDto user)
    {
        return Ok(await UsersService.RegisterWithToken(CurrentContext, user));
    }

}