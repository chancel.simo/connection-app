using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.Services;
using Swashbuckle.AspNetCore.Annotations;

namespace ConnectionAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AgencyController : BaseController
    {
        public AgencyController(DataContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper, configuration)
        {
        }

        [HttpGet("GetConnectedUserAgency")]
        [SwaggerOperation("GetConnectedUserAgency")]
        public async Task<ActionResult<AgencyResponse>> GetConnectedUserAgency()
        {
            return Ok(await AgenciesService.GetConnectedUserAgency(CurrentContext, Mapper));
        }

        [HttpGet("GetAgency")]
        [SwaggerOperation("GetAgency")]
        public async Task<ActionResult<AgencyResponse>> GetAgency(int AgencyID)
        {
            return Ok(await AgenciesService.GetAgency(CurrentContext, Mapper, AgencyID));
        }

        [HttpGet("GetAgencies")]
        [SwaggerOperation("GetAgencies")]
        public async Task<ActionResult<AgenciesResponse>> GetAgencies()
        {
            return Ok(await AgenciesService.GetAgencies(CurrentContext, Mapper));
        }

        [HttpPost("SaveAgency")]
        [SwaggerOperation("SaveAgency")]
        public async Task<ActionResult<AgencyResponse>> SaveAgency([FromBody] AgencyDto agency)
        {
            return Ok(await AgenciesService.SaveAgency(CurrentContext, Mapper, Configuration, agency));
        }

        [HttpPost("GetAgencyEmployees")]
        [SwaggerOperation("GetAgencyEmployees")]
        public async Task<ActionResult<UsersResponse>> GetAgencyEmployees([FromBody] GetUserRequest request)
        {
            return Ok(await UsersService.GetUsers(CurrentContext, Mapper, request));
        }
    }
}