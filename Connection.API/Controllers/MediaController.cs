using ConnectionAPI.DAL;
using AutoMapper;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Mvc;
using ConnectionAPI.Config;
using ConnectionAPI.Services;
using System.Net.Http.Headers;


namespace ConnectionAPI.Controllers
{

    public class UploadMediaResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
    }

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class MediaController : BaseController
    {
        public MediaController(DataContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper, configuration)
        {
        }

        [HttpPost]
        [SwaggerOperation("upload")]
        [SwaggerResponse(200, "", typeof(UploadMediaResponse))]
        public async Task<IActionResult> UploadMedia()
        {
            var response = new UploadMediaResponse();
            try
            {
                if (Request == null || Request.ContentLength == 0 || Request.Form == null || Request.Form.Files == null || Request.Form.Files.Count == 0)
                    throw new Exception("Impossible de récupérer les fichiers.");

                var file = Request.Form.Files[0];
                var tmpDirectory = Path.Combine(Server.SiteBaseDirectory, "Upload", "TempUpload");

                await GlobalService.CreateDirectoryIfNeededWithWriteAccess(tmpDirectory);

                string originalFileName = null, FilePath = null, FileName = null;

                if (file.Length > 0)
                {
                    originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string fileExtension = Path.GetExtension(originalFileName).Replace(".", "");
                    fileExtension = fileExtension.ToLower();
                    if (fileExtension == "jpeg")
                        fileExtension = "jpg";
                    var i = 1;
                    do
                    {
                        FileName = Guid.NewGuid().ToString() + (fileExtension != "" ? "." + fileExtension : "");
                        FilePath = Path.Combine(tmpDirectory, FileName);
                        i++;
                    } while (System.IO.File.Exists(FilePath));
                    using (var stream = new FileStream(FilePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                response.message = Newtonsoft.Json.JsonConvert.SerializeObject(new { filename = FileName, originalname = originalFileName });
                response.success = true;

            }
            catch (System.Exception ex)
            {
                response.message = ex.Message;
            }

            if (response.success)
                return Ok(response);
            return StatusCode(500);
        }
    }
}