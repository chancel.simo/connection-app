using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.Services;
using Swashbuckle.AspNetCore.Annotations;

namespace ConnectionAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AppDataController : BaseController
    {

        public AppDataController(DataContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper, configuration)
        {
        }

        [HttpGet("GetAppType")]
        [SwaggerOperation("GetAppType")]
        public async Task<ActionResult<GetAppTypeResponse>> GetAppType(string Code)
        {
            return Ok(await AppDataService.GetAppType(CurrentContext, Mapper, Code));
        }

        [HttpGet("GetCities")]
        [SwaggerOperation("GetCities")]
        public async Task<ActionResult<CitiesResponse>> GetCities()
        {
            return Ok(await AppDataService.GetCities(CurrentContext, Mapper));
        }
    }
}