using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL.DTO;
using ConnectionAPI.Services;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;
using ConnectionAPI.Config;

namespace ConnectionAPI.Controllers;

[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class ConnectionController : BaseController
{
    public ConnectionController(DataContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper, configuration)
    {
    }

    [HttpPost("GetConnectionList")]
    [SwaggerOperation("GetConnectionList")]
    // [Authorize(Constants.EmployeeRoleName)]
    public ActionResult<ConnectionsResponse> GetConnectionList([FromBody] ConnectionRequest request)
    {
        return Ok(ConnectionService.GetConnectionList(CurrentContext, Mapper, request));
    }

    [HttpGet("GetConnection")]
    [SwaggerOperation("GetConnection")]
    //[Authorize(Constants.EmployeeRoleName)]
    public ActionResult<ConnectionResponse> GetConnection(int ConnectionID)
    {
        return Ok(ConnectionService.GetConnection(CurrentContext, Mapper, ConnectionID));
    }

    [HttpPost("CreateConnection")]
    [SwaggerOperation("CreateConnection")]
    [Authorize(Constants.EmployeeRoleName)]
    public ActionResult<ConnectionResponse> CreateConnection([FromBody] ConnectionDto connection)
    {
        return Ok(ConnectionService.CreateConnection(CurrentContext, Mapper, connection));
    }

}