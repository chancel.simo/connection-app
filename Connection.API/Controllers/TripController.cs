using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ConnectionAPI.DAL;
using ConnectionAPI.DAL.Models;
using ConnectionAPI.DAL.DTO;
using Microsoft.EntityFrameworkCore;
using ConnectionAPI.Services;
using System.Security.Claims;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using AutoMapper;
using ConnectionAPI.Config;


namespace ConnectionAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TripController : BaseController
    {
        private readonly ILogger<TripController> _logger;

        public TripController(ILogger<TripController> logger, DataContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper, configuration)
        {
            _logger = logger;
        }

        [HttpPost("GetTripList")]
        [SwaggerOperation("GetTripList")]
        [Authorize(Constants.EmployeeRoleName)]
        public ActionResult<TripsResponse> GetTripsResponse([FromBody] TripRequest request)
        {
            return Ok(TripsService.GetTripsList(CurrentContext, Mapper, request));
        }

        [HttpPost("SaveTrip")]
        [SwaggerOperation("SaveTrip")]
        [Authorize("SuperAdmin")]
        public ActionResult<TripResponse> SaveTrip([FromBody] TripDto trip)
        {
            return Ok(TripsService.SaveTrip(CurrentContext, Mapper, trip));
        }


    }
}