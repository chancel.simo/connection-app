﻿using ConnectionAPI.Config;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;

namespace ConnectionAPI.utils
{
    public class MyHttpContext : IHttpContext
    {
        public HttpContext Current { get { return _httpContextAccessor.HttpContext; } }
        private IHttpContextAccessor _httpContextAccessor;
        private IConfiguration _configuration;
        private IWebHostEnvironment _webHost;
        public MyHttpContext(IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IWebHostEnvironment webHost)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _webHost = webHost;
            Server.CurrentContext = this;
        }
        public string CurrentUrl
        {
            get
            {
                if (Current != null)
                    return string.Concat(
                           Current.Request.Scheme,
                            "://",
                              Current.Request.Host.ToUriComponent(),
                              Current.Request.PathBase.ToUriComponent(),
                              Current.Request.Path.ToUriComponent(),
                              Current.Request.QueryString.ToUriComponent());
                return null;
            }
        }

        public string CurrentUrlReferer
        {
            get
            {
                return Current?.Request.Headers["Referer"].ToString();
            }
        }

        public void AddCookie(string key, string value, DateTime Expire)
        {
            var isDev = _webHost.IsDevelopment();

            Console.WriteLine("isdev : " + isDev);

            Current?.Response.Cookies.Append(key, value, new CookieOptions()
            {
                Expires = Expire,
                Domain = _webHost.IsDevelopment() ? "localhost" : "express-trip.com",
                SameSite = SameSiteMode.Strict
            });
        }

        public string GetCookie(string key)
        {
            return Current?.Request.Cookies[key];
        }

        public string GetIpAddress()
        {
            if (Current?.Request.Headers["X-Forwarded-For"].Count > 0)
                return Current?.Request.Headers["X-Forwarded-For"];
            return Current?.Connection.RemoteIpAddress.ToString();
        }

        public void Redirect(string url)
        {
            Current?.Response.Redirect(url);
        }

        public void RemoveCookie(string key)
        {
            Current?.Response.Cookies.Delete(key);
        }
        /* public bool IsLocalRequest()
        {
            if (Current != null)
                return Current.Request.IsLocal() || GetIpAddress() == Constants.ServerIpAddress;
            return false;
        } */

    }
}