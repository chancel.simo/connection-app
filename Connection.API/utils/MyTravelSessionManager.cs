using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConnectionAPI.Services;
using ConnectionAPI.Config;
using ConnectionAPI.DAL.DTO;
using Newtonsoft.Json;


namespace ConnectionAPI.utils
{
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) :
                JsonConvert.DeserializeObject<T>(value);
        }
    }
    public class MyTravelSession : ITravelSession
    {
        private IHttpContextAccessor _httpContextAccessor;

        public MyTravelSession(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public string SessionId
        {
            get
            {
                return _httpContextAccessor.HttpContext.Session.GetString(SessionKeysConstants.SessionId);
            }
            set
            {
                _httpContextAccessor.HttpContext.Session.SetString(SessionKeysConstants.SessionId, value);
            }
        }
        public UserDto CurrentUser
        {
            get
            {
                return _httpContextAccessor?.HttpContext?.Session?.Get<UserDto>(SessionKeysConstants.User);
            }
            set
            {
                _httpContextAccessor?.HttpContext?.Session?.Set<UserDto>(SessionKeysConstants.User, value);
            }
        }

        public bool SessionInitialized
        {
            get
            {
                return _httpContextAccessor.HttpContext.Session.GetInt32(SessionKeysConstants.SessionInitialized) == 1;
            }
            set
            {
                _httpContextAccessor.HttpContext.Session.SetInt32(SessionKeysConstants.SessionInitialized, value ? 1 : 0);
            }
        }

        /*  public AgentDTO ConnectAsRequester
         {
             get
             {
                 return _httpContextAccessor.HttpContext.Session.Get<AgentDTO>(Config.SessionKeysConstants.AgentConnectAsRequester);
             }
             set
             {
                 _httpContextAccessor.HttpContext.Session.Set<AgentDTO>(Config.SessionKeysConstants.AgentConnectAsRequester, value);
             }
         } */
    }

    public class MyTravelSessionManager : TravelSessionManager
    {
        private IHttpContextAccessor _httpContextAccessor;
        public MyTravelSessionManager(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public ITravelSession Instance
        {
            get
            {
                return new MyTravelSession(_httpContextAccessor);
            }
        }

        private static readonly object padlock = new object();
    }
}