
namespace ConnectionAPI.Config
{
    public interface IHttpContext
    {
        string CurrentUrl { get; }

        string CurrentUrlReferer { get; }

        string GetCookie(string key);

        void AddCookie(string key, string value, DateTime Expire);

        void RemoveCookie(string key);

        void Redirect(string url);

        string GetIpAddress();

        //   bool IsLocalRequest();
    }

    public static class Server
    {
        public static string SiteBaseUrl { get; set; }
        public static string ApiBaseUrl { get; set; }
        public static string SiteBaseDirectory { get; set; }
        public static IHttpContext CurrentContext { get; set; }

        public static string MapPath(string path)
        {
            return path.Replace("~", SiteBaseDirectory);
        }
    }
}