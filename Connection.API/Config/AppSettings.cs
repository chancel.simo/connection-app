
namespace ConnectionAPI.Config;

public static class AppSettings
{
    public static string SwaggerEndPoint = $"/swagger/v{1}/swagger.json";
    public static string APITitle = "CONNECTION API";
    public static bool IsDev { get; set; }
    public static bool IsProd { get; set; }
    public static bool MustSeedDataBase = false;
    public static string TravelCookieKey = "travel_refresh_token";
}
