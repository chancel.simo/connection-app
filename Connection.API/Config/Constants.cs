using System.ComponentModel;

namespace ConnectionAPI.Config
{
    public enum TransportType
    {
        [Description("Bus")]
        Bus = 1,

        [Description("Train")]
        Train = 2,

        [Description("Avion")]
        Avion = 3,

        [Description("AutoCar")]
        AutoCar = 4,
    }

    public static class Constants
    {
        public const string SuperAdminRoleName = "SuperAdmin";
        public const string AdminRoleName = "Admin";
        public const string EmployeeRoleName = "EmployeeRoleName";
    }

    public static class AppTypeConstant
    {
        public const string CityType = "CityType";

    }

    public static class SessionKeysConstants
    {
        public const string SessionId = "__SessionId__";
        public const string User = "__User__";
        public const string SessionInitialized = "__SessionInitialized__";

    }
}