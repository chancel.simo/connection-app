#stop pm2
pm2 stop connectionwebapi

#pull from distant repository
cd /home/chancel/applications/client-api/connection-app/

git pull

#build API

cd /home/chancel/applications/client-api/connection-app/Connection.API/

rm -rf published

dotnet publish -r linux-x64 --self-contained false -o published

cd published

mkdir Upload

cd Upload

#mkdir Public

pm2 start connectionwebapi
