//import { RoleDto } from "../connection-crm/src/services/api-client.generated";

export class AppTypeConstant {
    public static CityType = "CityType";
}
export enum RoleList {
    SuperAdminRoleName = "super-admin",
    AdminRoleName = "admin",
    EmployeeRoleName = "employé"
}

export class Constant {
    public static defaultRoles = [
        {
            RoleCode: "SuperAdmin",
            RoleName: "super-admin"
        },
        {
            RoleCode: "Admin",
            RoleName: "admin"
        },
        {
            RoleCode: "Employee",
            RoleName: "employé"
        },
        {
            RoleCode: "Client",
            RoleName: "client"
        }
    ];
}