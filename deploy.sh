#stop pm2
pm2 stop connectionwebapi

#pull from distant repository
cd /home/chancel/applications/client-api/connection-app/

git pull

#build appli-web 

cd /home/chancel/applications/client-api/connection-app/connection-web/

ng build --configuration production

#build crm-appli

cd /home/chancel/applications/client-api/connection-app/connection-crm/

ng build --configuration production

#build API

cd /home/chancel/applications/client-api/connection-app/Connection.API/

dotnet publish -r linux-x64 --self-contained false -o published

pm2 start connectionwebapi
